package cz.jlochman.marklar.vlak_vis.experiment;

import cz.jlochman.marklar.core.logicModules.observer.LogicModuleEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class TempReadOutEvent extends LogicModuleEvent {

   @Getter
   private final double temperature;

}
