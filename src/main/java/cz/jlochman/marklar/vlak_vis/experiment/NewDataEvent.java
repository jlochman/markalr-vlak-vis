package cz.jlochman.marklar.vlak_vis.experiment;

import cz.jlochman.marklar.core.logicModules.observer.LogicModuleEvent;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataRow;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class NewDataEvent extends LogicModuleEvent {

   @Getter
   private final VlakVisDataRow dataRow;

}
