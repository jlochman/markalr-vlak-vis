package cz.jlochman.marklar.vlak_vis.experiment;

public enum RunningSubstatus {

   INITIAL_TEMP,

   INCREASING_TEMP,

   DECREASING_TEMP,

   FINISHED;

}
