package cz.jlochman.marklar.vlak_vis.experiment.calculator;

import java.util.List;
import java.util.stream.Collectors;

import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataRow;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;
import cz.jlochman.marklar.vlak_vis.experiment.settings.VlakVisInstanceSettingsService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

public class VlakVisCalculator {

   private VlakVisInstanceSettingsService instanceSS;
   private VlakVisDataService             dataService;

   @Getter
   private double hcht, dcht, trt;
   
   public VlakVisCalculator(ExperimentInstance expInstance) {
      if (expInstance == null) {
         throw new NullPointerException("null expInstance");
      }
      this.instanceSS =
            (VlakVisInstanceSettingsService) expInstance.getSettignsService();
      this.dataService = (VlakVisDataService) expInstance.getDataService();
   }

   public void calculate() {
      RegressCoef rc = getRegressCoef(dataService.getDataRows());
      if (rc == null || rc.getK() == 0) {
         return;
      }
      double logR = getLogR();
      
      hcht = (logR - rc.getQ()) / rc.getK();
      dcht = (logR - 1.5 - rc.getQ()) / rc.getK();
      trt = (logR - 0.3 - rc.getQ()) / rc.getK();
   }
   
   public void save() {
      dataService.saveHcht(hcht);
      dataService.saveDcht(dcht);
      dataService.saveTrt(trt);
   }
   
   private double getLogR() {
      double kki = dataService.loadKki();
      double ksi = dataService.loadKsi();
      double d = instanceSS.getDiameter();
      
      if (d == 0) {
         return 0;
      }
      double dldt = kki * (1/(d*d) - ksi);
      if (dldt <= 0) {
         return 0;
      }
      return Math.log10(dldt);
   }

   private final static double MIN_SPEED_MM_MIN = 0.1;
   private final static double MAX_SPEED_MM_MIN = 0.6;

   private RegressCoef getRegressCoef(List<VlakVisDataRow> dataRows) {
      if (dataRows == null || dataRows.isEmpty()) {
         return null;
      }
      dataRows = dataRows.stream()
                         .filter(dr -> dr.getSpeedMmMin() >= MIN_SPEED_MM_MIN
                                       && dr.getSpeedMmMin() <= MAX_SPEED_MM_MIN)
                         .collect(Collectors.toList());
      if (dataRows.isEmpty()) {
         return null;
      }
      double s1 = 0, s2 = 0, s3 = 0, s4 = 0;
      double n = dataRows.size();
      for (VlakVisDataRow dr : dataRows) {
         double x = dr.getTemp();
         double y = dr.getSpeedLogMmMin();
         s1 += x * y;
         s2 += x;
         s3 += y;
         s4 += x * x;
      }

      double k = (s1 - s2 * s3 / n) / (s4 - s2 * s2 / n);
      double q = (s3 - k * s2) / n;
      return new RegressCoef(k, q);
   }

   // log(dl/dt) = k * t + q
   @Data
   @AllArgsConstructor
   private class RegressCoef {

      private double k, q;
   }

}
