package cz.jlochman.marklar.vlak_vis.experiment;

import java.util.List;

import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.logicModules.definitions.ExperimentType;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;
import cz.jlochman.marklar.vlak_vis.experiment.settings.VlakVisExpTypeSettingsService;
import cz.jlochman.marklar.vlak_vis.experiment.settings.VlakVisInstanceSettingsService;
import cz.jlochman.marklar.vlak_vis.export.VlakVisExportService;
import cz.jlochman.marklar.vlak_vis.graphics.VlakVisGraphicFactory;

public class VlakVisExperimentType extends ExperimentType {

   public VlakVisExperimentType() {
      setName("Vláknový Vyskozimetr");
      setDescription(getName());
   }

   @Override
   public VlakVisDataService getDataService(ExperimentInstance instance) {
      return new VlakVisDataService(instance);
   }

   @Override
   public VlakVisExperimentLogic getExperimentLogic() {
      return new VlakVisExperimentLogic();
   }

   private VlakVisGraphicFactory graphicFactory = null;

   @Override
   public VlakVisGraphicFactory getGraphicFactory() {
      if (graphicFactory == null) {
         graphicFactory = new VlakVisGraphicFactory();
      }
      return graphicFactory;
   }

   private VlakVisExpTypeSettingsService expTypeSS = null;

   @Override
   public VlakVisExpTypeSettingsService getExpDefaultSettingsService() {
      if (expTypeSS == null) {
         expTypeSS = new VlakVisExpTypeSettingsService(this);
      }
      return expTypeSS;
   }

   @Override
   public VlakVisInstanceSettingsService
         getExpInstanceSettignsService(ExperimentInstance expInstance) {
      return new VlakVisInstanceSettingsService(expInstance);
   }

   @Override
   public VlakVisExportService
         getExperimentExportService(List<ExperimentInstance> expInstances) {
      return new VlakVisExportService(expInstances);
   }

}
