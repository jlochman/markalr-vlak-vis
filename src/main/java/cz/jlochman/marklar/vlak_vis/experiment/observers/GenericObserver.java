package cz.jlochman.marklar.vlak_vis.experiment.observers;

import java.io.IOException;

import cz.jlochman.marklar.core.components.valueChecker.exceptions.GetValueException;
import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrument;

public abstract class GenericObserver {

   protected ExperimentInstance expInstance;
   protected VlakVisInstrument  instrument;

   public GenericObserver(ExperimentInstance expInstance) {
      this.expInstance = expInstance;
      instrument = (VlakVisInstrument) expInstance.getInstrument();
   }

   public abstract void readValue()
         throws IOException, GetValueException, Exception;

}
