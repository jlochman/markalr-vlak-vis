package cz.jlochman.marklar.vlak_vis.experiment.settings;

public interface IVlakVisSettings {

   long getIntervalReadOut();

   void setIntervalReadOut(long intervalMilis);

   long getIntervalPersist();

   void setIntervalPersist(long intervalMilis);

   double getStartT();

   void setStartT(double temp);

   double getLength();

   void setLength(double length);

   double getDiameter();

   void setDiameter(double diameter);

   double getSpeedTurningPoint();

   void setSpeedTurningPoint(double turningPointMmMin);

}
