package cz.jlochman.marklar.vlak_vis.experiment;

import cz.jlochman.marklar.core.ServiceLocator;
import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.logicModules.definitions.ExperimentLogic;
import cz.jlochman.marklar.core.logicModules.logicEnums.ChangingStatusReturnCode;
import cz.jlochman.marklar.core.logicModules.observer.ExperimentLogicListener;
import cz.jlochman.marklar.core.logicModules.observer.LogicModuleEvent;
import cz.jlochman.marklar.fxcommon.logger.service.LogService;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataRow;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;
import cz.jlochman.marklar.vlak_vis.experiment.calculator.VlakVisCalculator;
import cz.jlochman.marklar.vlak_vis.experiment.observers.LengthSpeedObserver;
import cz.jlochman.marklar.vlak_vis.experiment.observers.TemperatureObserver;
import cz.jlochman.marklar.vlak_vis.experiment.settings.VlakVisInstanceSettingsService;
import cz.jlochman.marklar.vlak_vis.graphics.settings.PowerRestriction;
import cz.jlochman.marklar.vlak_vis.graphics.settings.PowerRestrictionEntry;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrument;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrumentSettingsService;
import lombok.extern.log4j.Log4j;

@Log4j
public class VlakVisExperimentLogic extends ExperimentLogic {

   private VlakVisInstrument                vlakVisInstrument;
   private VlakVisInstrumentSettingsService vlakVisInstrumentSS;
   private VlakVisDataService               vlakVisDataService;
   private VlakVisInstanceSettingsService   vlakVisInstanceSS;

   private RunningSubstatus runningSubStatus;

   private long intervalReadOut, intervalPersist;
   private long lastReadOutTick, lastPersistTick;

   private PowerRestriction powRestriciton;

   private int              consecutiveErrorsCount;
   private final static int MAX_CONSECUTIVE_ERRORS = 5;

   private double startTemp;
   private double startLengthMm;

   private TemperatureObserver tempObserver;
   private LengthSpeedObserver lengthSpeedObserver;

   private final static double ABOVE_START_T = 200.;
   private final static double BELOW_START_T = 150.;
   
   private final static int NUM_TRIES = 5;

   private double              speedMmMinTurn;
   private final static double SPEED_MM_MIN_TERMINATE = 0.1;

   private ExperimentLogicListener expInstanceListener;

   @Override
   public void setExpInstance(ExperimentInstance expInstance) {
      super.setExpInstance(expInstance);

      vlakVisInstrument = (VlakVisInstrument) instrument;
      vlakVisInstrumentSS =
            (VlakVisInstrumentSettingsService) instrument.getSettingsService();
      vlakVisDataService = (VlakVisDataService) expInstance.getDataService();
      vlakVisInstanceSS =
            (VlakVisInstanceSettingsService) expInstance.getSettignsService();

      tempObserver = new TemperatureObserver(expInstance);
      lengthSpeedObserver = new LengthSpeedObserver(expInstance);

      expInstanceListener = new ExperimentLogicListener() {

         @Override
         public void processEvent(LogicModuleEvent event) {
            if (event instanceof NewRunningSubStatusEvent) {
               NewRunningSubStatusEvent pevent =
                     (NewRunningSubStatusEvent) event;
               runningSubStatus = pevent.getRunningSubstatus();
               vlakVisInstanceSS.setRunningSubstatus(runningSubStatus);
               switch (runningSubStatus) {
               case INITIAL_TEMP:
                  setActualExperimentTime(System.currentTimeMillis());
                  initInitialTemp();
                  break;
               case INCREASING_TEMP:
                  setActualExperimentTime(System.currentTimeMillis());
                  initIncreasingTemp();
                  break;
               case DECREASING_TEMP:
                  setActualExperimentTime(System.currentTimeMillis());
                  initDecreasingTemp();
                  break;
               case FINISHED:
                  terminate();
                  break;
               }
            }
         }
      };
      ServiceLocator.getInstance()
                    .getLogicModuleManager()
                    .registerListener(expInstance, expInstanceListener);
   }

   @Override
   protected void finalize() throws Throwable {
      super.finalize();
      if (expInstanceListener != null) {
         ServiceLocator.getInstance()
                       .getLogicModuleManager()
                       .unregisterListener(expInstance, expInstanceListener);
      }
   }

   @Override
   public ChangingStatusReturnCode postAbortedRun() {
      changeRunningSubStatus(RunningSubstatus.FINISHED);
      return ChangingStatusReturnCode.OK;
   }

   @Override
   public ChangingStatusReturnCode postChashed() {
      return init();
   }

   @Override
   public ChangingStatusReturnCode postPause() {
      return init();
   }

   @Override
   public ChangingStatusReturnCode postRun() {
      terminate();
      return ChangingStatusReturnCode.OK;
   }

   @Override
   public ChangingStatusReturnCode postSuspend() {
      return ChangingStatusReturnCode.OK;
   }

   @Override
   public ChangingStatusReturnCode prePause() {
      return ChangingStatusReturnCode.OK;
   }

   @Override
   public ChangingStatusReturnCode preRun() {
      return init();
   }

   @Override
   public ChangingStatusReturnCode preSuspend() {
      terminate();
      return ChangingStatusReturnCode.OK;
   }

   @Override
   public ChangingStatusReturnCode restore() {
      return ChangingStatusReturnCode.OK;
   }

   @Override
   public ChangingStatusReturnCode runLogic() {
      while (true) {

         if (!running) {
            break;
         }

         try {
            Thread.sleep(100L);
         } catch (InterruptedException e) {
            Thread.currentThread()
                  .interrupt();
         }

         try {
            switch (runningSubStatus) {
            case INITIAL_TEMP:
               if (System.currentTimeMillis()
                   - lastReadOutTick > intervalReadOut) {
                  lastReadOutTick = System.currentTimeMillis();
                  tempObserver.readValue();
                  lengthSpeedObserver.readValue();
               }
               break;
            case INCREASING_TEMP:
               if (System.currentTimeMillis()
                   - lastReadOutTick > intervalReadOut) {
                  lastReadOutTick = System.currentTimeMillis();
                  tempObserver.readValue();
                  lengthSpeedObserver.readValue();
                  if (lengthSpeedObserver.getSpeedMmMin() >= speedMmMinTurn) {
                     changeRunningSubStatus(RunningSubstatus.DECREASING_TEMP);
                  }
               }
               break;
            case DECREASING_TEMP:
               if (System.currentTimeMillis()
                   - lastReadOutTick > intervalReadOut) {
                  lastReadOutTick = System.currentTimeMillis();
                  tempObserver.readValue();
                  lengthSpeedObserver.readValue();
                  if (System.currentTimeMillis()
                      - lastPersistTick > intervalPersist) {
                     lastPersistTick = System.currentTimeMillis();

                     VlakVisDataRow dataRow =
                           new VlakVisDataRow(getActualExperimentTime());
                     dataRow.setTemp(tempObserver.getTemperature());
                     dataRow.setLengthMm(startLengthMm
                                         + lengthSpeedObserver.getLengthMm());
                     dataRow.setSpeedMmMin(lengthSpeedObserver.getSpeedMmMin());
                     dataRow.setSpeedLogMmMin(lengthSpeedObserver.getSpeedLogMmMin());
                     vlakVisDataService.saveDataRow(dataRow);
                     raiseExpInstanceEvent(new NewDataEvent(dataRow));
                  }

                  if (lengthSpeedObserver.getSpeedMmMin() < SPEED_MM_MIN_TERMINATE) {
                     changeRunningSubStatus(RunningSubstatus.FINISHED);
                  }

               }
               break;
            case FINISHED:
               if (expInstance != null && expInstance.getId() != null) {
                  expInstance = ServiceLocator.getInstance()
                                              .getExperimentService()
                                              .getExperimentByID(expInstance.getId());
                  VlakVisCalculator calc = new VlakVisCalculator(expInstance);
                  calc.calculate();
                  calc.save();
               }
               return ChangingStatusReturnCode.SUCCESSFUL_END;
            }
            consecutiveErrorsCount = 0;
         } catch (Exception e) {
            e.printStackTrace();
            consecutiveErrorsCount++;
         }

         if (consecutiveErrorsCount > MAX_CONSECUTIVE_ERRORS) {
            log.error("max consecutive errors occured");
            return ChangingStatusReturnCode.ERROR;
         }

      }

      return ChangingStatusReturnCode.OK;
   }

   private void changeRunningSubStatus(RunningSubstatus newStatus) {
      raiseExpInstanceEvent(new NewRunningSubStatusEvent(newStatus));
   }

   private boolean terminateCalled = false;

   private void terminate() {
      if (terminateCalled) {
         return;
      }
      if (expInstance != null && expInstance.getId() != null) {
         expInstance = ServiceLocator.getInstance()
                                     .getExperimentService()
                                     .getExperimentByID(expInstance.getId());
         VlakVisCalculator calc = new VlakVisCalculator(expInstance);
         calc.calculate();
         calc.save();
      }
      changeRunningSubStatus(RunningSubstatus.FINISHED);
      terminateCalled = true;
   }

   private ChangingStatusReturnCode init() {
      if (!vlakVisInstrument.connect()) {
         LogService.getInstance()
                   .error("Nanodac not connected");
         return ChangingStatusReturnCode.ERROR;
      }

      lastReadOutTick = System.currentTimeMillis();
      lastPersistTick = System.currentTimeMillis();
      consecutiveErrorsCount = 0;

      startTemp = vlakVisInstanceSS.getStartT();
      startLengthMm = vlakVisInstanceSS.getLength();
      speedMmMinTurn = vlakVisInstanceSS.getSpeedTurningPoint();

      intervalReadOut = vlakVisInstanceSS.getIntervalReadOut();
      intervalPersist = vlakVisInstanceSS.getIntervalPersist();
      runningSubStatus = vlakVisInstanceSS.getRunningSubstatus();

      powRestriciton = vlakVisInstrumentSS.getPowerRestriction();

      ChangingStatusReturnCode changingStatus;
      switch (runningSubStatus) {
      case INITIAL_TEMP:
         changingStatus = initInitialTemp();
         if (changingStatus == ChangingStatusReturnCode.OK) {
            changeRunningSubStatus(RunningSubstatus.INITIAL_TEMP);
         } else {
            return changingStatus;
         }
         break;
      case INCREASING_TEMP:
         changingStatus = initIncreasingTemp();
         if (changingStatus == ChangingStatusReturnCode.OK) {
            changeRunningSubStatus(RunningSubstatus.INCREASING_TEMP);
         } else {
            return changingStatus;
         }
         break;
      case DECREASING_TEMP:
         changingStatus = initDecreasingTemp();
         if (changingStatus == ChangingStatusReturnCode.OK) {
            changeRunningSubStatus(RunningSubstatus.DECREASING_TEMP);
         } else {
            return changingStatus;
         }
         break;
      case FINISHED:
         changingStatus = initFinished();
         if (changingStatus == ChangingStatusReturnCode.OK) {
            changeRunningSubStatus(RunningSubstatus.FINISHED);
         } else {
            return changingStatus;
         }
         break;
      }
      return ChangingStatusReturnCode.OK;
   }

   private ChangingStatusReturnCode initInitialTemp() {
      if (!vlakVisInstrument.connect()) {
         LogService.getInstance()
                   .error("Nanodac not connected");
         return ChangingStatusReturnCode.ERROR;
      }
      
      try {
         if (vlakVisInstrument != null) {
            vlakVisInstrument.setTempSetpoint(startTemp, NUM_TRIES);
            vlakVisInstrument.setOutLowLimit(0, NUM_TRIES);
            vlakVisInstrument.setOutHighLimit(80, NUM_TRIES);
         }
      } catch (Exception e) {
         LogService.getInstance()
                   .error("Error setting initial temperature");
         e.printStackTrace();
         return ChangingStatusReturnCode.ERROR;
      }
      return ChangingStatusReturnCode.OK;
   }

   private ChangingStatusReturnCode initIncreasingTemp() {
      if (!vlakVisInstrument.connect()) {
         LogService.getInstance()
                   .error("Nanodac not connected");
         return ChangingStatusReturnCode.ERROR;
      }
      
      try {
         if (vlakVisInstrument != null) {
            vlakVisInstrument.setTempSetpoint(startTemp + ABOVE_START_T, NUM_TRIES);

            PowerRestrictionEntry restriction =
                  powRestriciton.getRestriction(startTemp);
            vlakVisInstrument.setOutLowLimit(restriction.getLowLimit(), NUM_TRIES);
            vlakVisInstrument.setOutHighLimit(restriction.getHighLimit(), NUM_TRIES);
         }
      } catch (Exception e) {
         LogService.getInstance()
                   .error("Error setting temp setpoint");
         e.printStackTrace();
         return ChangingStatusReturnCode.ERROR;
      }
      return ChangingStatusReturnCode.OK;
   }

   private ChangingStatusReturnCode initDecreasingTemp() {
      if (!vlakVisInstrument.connect()) {
         LogService.getInstance()
                   .error("Nanodac not connected");
         return ChangingStatusReturnCode.ERROR;
      }
      
      try {
         if (vlakVisInstrument != null) {
            vlakVisInstrument.setTempSetpoint(startTemp - BELOW_START_T, NUM_TRIES);

            PowerRestrictionEntry restriction =
                  powRestriciton.getRestriction(startTemp);
            vlakVisInstrument.setOutLowLimit(restriction.getLowLimit(), NUM_TRIES);
            vlakVisInstrument.setOutHighLimit(restriction.getHighLimit(), NUM_TRIES);
         }
      } catch (Exception e) {
         LogService.getInstance()
                   .error("Error setting temp ramp");
         e.printStackTrace();
         return ChangingStatusReturnCode.ERROR;
      }
      return ChangingStatusReturnCode.OK;
   }

   private ChangingStatusReturnCode initFinished() {
      if (!vlakVisInstrument.connect()) {
         LogService.getInstance()
                   .error("Nanodac not connected");
         return ChangingStatusReturnCode.ERROR;
      }
      
      try {
         if (vlakVisInstrument != null) {
            vlakVisInstrument.setTempSetpoint(startTemp, NUM_TRIES);
            vlakVisInstrument.setOutLowLimit(0, NUM_TRIES);
            vlakVisInstrument.setOutHighLimit(80, NUM_TRIES);
         }
      } catch (Exception e) {
         LogService.getInstance()
                   .error("Error setting temp ramp");
         e.printStackTrace();
         return ChangingStatusReturnCode.ERROR;
      }
      return ChangingStatusReturnCode.OK;
   }

}
