package cz.jlochman.marklar.vlak_vis.experiment.settings;

import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.logicModules.settingsServices.ExpInstanceSettingsService;
import cz.jlochman.marklar.core.logicModules.settingsServices.exceptions.ValueNotFoundException;
import cz.jlochman.marklar.vlak_vis.experiment.RunningSubstatus;

public class VlakVisInstanceSettingsService extends ExpInstanceSettingsService
      implements IVlakVisSettings {

   private final static String READ_OUT_INTERVAL = "readOutInterval";
   private final static String PERSIST_INTERVAL  = "persistInterval";

   private final static String START_T  = "startT";
   private final static String LENGTH   = "length";
   private final static String DIAMETER = "diameter";

   private final static String RUNNING_SUBSTATUS   = "runSubStatus";
   private final static String SPEED_TURNING_POINT = "speedTP";

   private VlakVisExpTypeSettingsService defaultSS;

   public VlakVisInstanceSettingsService(ExperimentInstance expInstance) {
      super(expInstance);
      defaultSS =
            (VlakVisExpTypeSettingsService) expInstance.getExperimentType()
                                                       .getExpDefaultSettingsService();
   }

   @Override
   public long getIntervalReadOut() {
      try {
         return loadLongValue(READ_OUT_INTERVAL);
      } catch (ValueNotFoundException e) {
         return defaultSS.getIntervalReadOut();
      }
   }

   @Override
   public void setIntervalReadOut(long intervalMilis) {
      saveLongValue(intervalMilis, READ_OUT_INTERVAL);
   }

   @Override
   public long getIntervalPersist() {
      try {
         return loadLongValue(PERSIST_INTERVAL);
      } catch (ValueNotFoundException e) {
         return defaultSS.getIntervalPersist();
      }
   }

   @Override
   public void setIntervalPersist(long intervalMilis) {
      saveLongValue(intervalMilis, PERSIST_INTERVAL);
   }

   @Override
   public double getStartT() {
      try {
         return loadDoubleValue(START_T);
      } catch (ValueNotFoundException e) {
         return defaultSS.getStartT();
      }
   }

   @Override
   public void setStartT(double temp) {
      saveDoubleValue(temp, START_T);
   }

   @Override
   public double getLength() {
      try {
         return loadDoubleValue(LENGTH);
      } catch (ValueNotFoundException e) {
         return defaultSS.getLength();
      }
   }

   @Override
   public void setLength(double length) {
      saveDoubleValue(length, LENGTH);
   }

   @Override
   public double getDiameter() {
      try {
         return loadDoubleValue(DIAMETER);
      } catch (ValueNotFoundException e) {
         return defaultSS.getDiameter();
      }
   }

   @Override
   public void setDiameter(double diameter) {
      saveDoubleValue(diameter, DIAMETER);
   }

   public RunningSubstatus getRunningSubstatus() {
      try {
         return RunningSubstatus.valueOf(loadStringValue(RUNNING_SUBSTATUS));
      } catch (Exception e) {
         return RunningSubstatus.INITIAL_TEMP;
      }
   }

   public void setRunningSubstatus(RunningSubstatus substatus) {
      if (substatus == null) {
         saveStringValue("NULL", RUNNING_SUBSTATUS);
      } else {
         saveStringValue(substatus.name(), RUNNING_SUBSTATUS);
      }
   }

   @Override
   public double getSpeedTurningPoint() {
      try {
         return loadDoubleValue(SPEED_TURNING_POINT);
      } catch (ValueNotFoundException e) {
         return defaultSS.getSpeedTurningPoint();
      }
   }

   @Override
   public void setSpeedTurningPoint(double turningPointMmMin) {
      saveDoubleValue(turningPointMmMin, SPEED_TURNING_POINT);
   }

}
