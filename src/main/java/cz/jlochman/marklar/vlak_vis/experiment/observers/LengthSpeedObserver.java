package cz.jlochman.marklar.vlak_vis.experiment.observers;

import java.io.IOException;

import cz.jlochman.marklar.core.components.valueChecker.exceptions.GetValueException;
import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.logicModules.manager.LogicEventUtility;
import cz.jlochman.marklar.vlak_vis.experiment.LengthSpeedReadOutEvent;
import lombok.Getter;

public class LengthSpeedObserver extends GenericObserver {

   @Getter
   private double lengthMm, speedMmMin;
   
   private final static int NUM_TRIES = 3;

   public LengthSpeedObserver(ExperimentInstance expInstance) {
      super(expInstance);
   }

   @Override
   public void readValue() throws IOException, GetValueException, Exception {
      if (!instrument.connect()) {
         return;
      }
      lengthMm = instrument.getLength(NUM_TRIES);
      speedMmMin = instrument.getSpeed();
      LogicEventUtility.raiseEvent(new LengthSpeedReadOutEvent(lengthMm,
                                                               speedMmMin),
                                   expInstance);

   }

   public double getSpeedLogMmMin() {
      if (speedMmMin == 0) {
         return 0;
      }
      return Math.log10(Math.abs(speedMmMin));
   }

}
