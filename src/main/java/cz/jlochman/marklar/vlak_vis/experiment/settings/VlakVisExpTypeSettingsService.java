package cz.jlochman.marklar.vlak_vis.experiment.settings;

import cz.jlochman.marklar.core.logicModules.definitions.ExperimentType;
import cz.jlochman.marklar.core.logicModules.settingsServices.ExpTypeSettingsService;
import cz.jlochman.marklar.core.logicModules.settingsServices.exceptions.ValueNotFoundException;

public class VlakVisExpTypeSettingsService extends ExpTypeSettingsService
      implements IVlakVisSettings {

   private final static String READ_OUT_INTERVAL = "readOutInterval";
   private final static String PERSIST_INTERVAL  = "persistInterval";

   private final static String START_T  = "startT";
   private final static String LENGTH   = "length";
   private final static String DIAMETER = "diameter";

   private final static String SPEED_TURNING_POINT = "speedTP";

   public VlakVisExpTypeSettingsService(ExperimentType expType) {
      super(expType);
   }

   @Override
   public long getIntervalReadOut() {
      try {
         return loadLongValue(READ_OUT_INTERVAL);
      } catch (ValueNotFoundException e) {
         return 1000L;
      }
   }

   @Override
   public void setIntervalReadOut(long intervalMilis) {
      saveLongValue(intervalMilis, READ_OUT_INTERVAL);
   }

   @Override
   public long getIntervalPersist() {
      try {
         return loadLongValue(PERSIST_INTERVAL);
      } catch (ValueNotFoundException e) {
         return 5000L;
      }
   }

   @Override
   public void setIntervalPersist(long intervalMilis) {
      saveLongValue(intervalMilis, PERSIST_INTERVAL);
   }

   @Override
   public double getStartT() {
      try {
         return loadDoubleValue(START_T);
      } catch (ValueNotFoundException e) {
         return 300.;
      }
   }

   @Override
   public void setStartT(double temp) {
      saveDoubleValue(temp, START_T);
   }

   @Override
   public double getLength() {
      try {
         return loadDoubleValue(LENGTH);
      } catch (ValueNotFoundException e) {
         return 300.;
      }
   }

   @Override
   public void setLength(double length) {
      saveDoubleValue(length, LENGTH);
   }

   @Override
   public double getDiameter() {
      try {
         return loadDoubleValue(DIAMETER);
      } catch (ValueNotFoundException e) {
         return 10.;
      }
   }

   @Override
   public void setDiameter(double diameter) {
      saveDoubleValue(diameter, DIAMETER);
   }

   @Override
   public double getSpeedTurningPoint() {
      try {
         return loadDoubleValue(SPEED_TURNING_POINT);
      } catch (ValueNotFoundException e) {
         return 0.6;
      }
   }

   @Override
   public void setSpeedTurningPoint(double turningPointMmMin) {
      saveDoubleValue(turningPointMmMin, SPEED_TURNING_POINT);
   }

}
