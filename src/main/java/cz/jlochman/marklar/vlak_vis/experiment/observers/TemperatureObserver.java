package cz.jlochman.marklar.vlak_vis.experiment.observers;

import java.io.IOException;

import cz.jlochman.marklar.core.components.valueChecker.exceptions.GetValueException;
import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.logicModules.manager.LogicEventUtility;
import cz.jlochman.marklar.vlak_vis.experiment.TempReadOutEvent;
import lombok.Getter;

public class TemperatureObserver extends GenericObserver {

   @Getter
   private double temperature;
   
   private final static int NUM_TRIES = 3;

   public TemperatureObserver(ExperimentInstance expInstance) {
      super(expInstance);
   }

   @Override
   public void readValue() throws IOException, GetValueException, Exception {
      if (!instrument.connect()) {
         return;
      }
      temperature = instrument.getTemperature(NUM_TRIES);
      LogicEventUtility.raiseEvent(new TempReadOutEvent(temperature),
                                   expInstance);

   }

}
