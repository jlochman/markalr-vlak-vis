package cz.jlochman.marklar.vlak_vis.utils;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

public class ListViewUtils {

   /**
    * Prevod generickeho objektu na string
    *
    * @param <T>
    */
   public static interface ToStringConverter<T> {

      String toString(T t);
   }

   /**
    * Vybrani generickeho objektu
    *
    * @param <T>
    */
   public static interface NewItemSelectedEvent<T> {

      void processNewItem(T newItem);
   }

   /**
    * Prirazeni, jak se maji objekty, ktere ListView obsahuje, prevadet na
    * string
    * 
    * @param listView
    *           tomuto listView se ma converter priradit
    * @param converter
    *           popisuje prevod objektu na string
    */
   public static <T> void setCellFactory(ListView<T> listView,
                                         ToStringConverter<T> converter) {
      listView.setCellFactory(lw -> new ListCell<T>() {

         @Override
         protected void updateItem(T item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
               setText(null);
            } else {
               setText(converter.toString(item));
            }
         }
      });
   }

   /**
    * Prirazeni, jak se maji objekty, ktere ListView obsahuje, prevadet na
    * String a jake kontextove menu maji vyvolavat
    * 
    * @param listView
    *           tomtuo listView se ma converter priradit
    * @param converter
    *           popisuje prevod objektu na string
    * @param contextMenu
    *           popisuje kontextove menu, co se ma vyvolavat
    */
   public static <T> void setCellFactory(ListView<T> listView,
                                         ToStringConverter<T> converter,
                                         ContextMenu contextMenu) {
      listView.setCellFactory(lv -> {

         ListCell<T> cell = new ListCell<T>() {

            @Override
            protected void updateItem(T item, boolean empty) {
               super.updateItem(item, empty);
               if (empty) {
                  setText(null);
               } else {
                  setText(converter.toString(item));
               }
            }
         };

         cell.emptyProperty()
             .addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                   cell.setContextMenu(null);
                } else {
                   cell.setContextMenu(contextMenu);
                }
             });

         return cell;
      });
   }

   /**
    * Prirazeni k zadanemu listView, co se ma stat, pokud je nejaky item vybran
    * - at uz stiskem mysi, sipkama...
    * 
    * @param listView
    *           k tomuto listView se listener priradi
    * @param event
    *           co se ma stat s nove vybranym itememS
    */
   public static <T> void
         addItemSelectedListener(ListView<T> listView,
                                 NewItemSelectedEvent<T> event) {
      listView.getSelectionModel()
              .selectedItemProperty()
              .addListener(new ChangeListener<T>() {

                 @Override
                 public void changed(ObservableValue<? extends T> observable,
                                     T oldValue, T newValue) {
                    event.processNewItem(newValue);
                 }
              });
   }

}
