package cz.jlochman.marklar.vlak_vis.data;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.entityDomain.ExperimentInstanceTimedData;
import cz.jlochman.marklar.core.experiments.service.ExperimentInstanceDataService;

public class VlakVisDataService extends ExperimentInstanceDataService {

   private final static String DATA_ROW = "DR";
   private final static String KKI = "KK(i)";
   private final static String KSI = "KS(i)";
   
   private final static String HCHT = "HCHT";
   private final static String DCHT = "DCHT";
   private final static String TRT = "TRT";

   public VlakVisDataService(ExperimentInstance instance) {
      super(instance);
   }

   public void saveDataRow(VlakVisDataRow dataRow) {
      addTimedStringValue(dataRow.asString(), DATA_ROW, dataRow.getTime());
   }

   public List<VlakVisDataRow> getDataRows() {
      List<VlakVisDataRow> dataRows = new ArrayList<>();

      List<ExperimentInstanceTimedData> timedData = instance.getTimedData();
      for (ExperimentInstanceTimedData eiTD : timedData) {
         VlakVisDataRow dataRow = VlakVisDataRow.fromString(eiTD.getValue(), eiTD.getExperimentTime());
         dataRows.add(dataRow);
      }

      return dataRows.stream()
                     .sorted(Comparator.comparing(VlakVisDataRow::getTime))
                     .collect(Collectors.toList());
   }
   
   public void saveKki(double kki) {
      saveFinalDoubleValue(kki, KKI);
   }
   
   public double loadKki() {
      return loadSingleDoubleValue(KKI);
   }
   
   public void saveKsi(double ksi) {
      saveFinalDoubleValue(ksi, KSI);
   }
   
   public double loadKsi() {
      return loadSingleDoubleValue(KSI);
   }
   
   public void saveHcht(double hcht) {
      saveFinalDoubleValue(hcht, HCHT);
   }
   
   public double loadHcht() {
      return loadSingleDoubleValue(HCHT);
   }
   
   public void saveDcht(double dcht) {
      saveFinalDoubleValue(dcht, DCHT);
   }
   
   public double loadDcht() {
      return loadSingleDoubleValue(DCHT);
   }
   
   public void saveTrt(double trt) {
      saveFinalDoubleValue(trt, TRT);
   }
   
   public double loadTrt() {
      return loadSingleDoubleValue(TRT);
   }

}
