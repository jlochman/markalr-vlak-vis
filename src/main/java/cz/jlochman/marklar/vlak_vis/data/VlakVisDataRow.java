package cz.jlochman.marklar.vlak_vis.data;

import cz.jlochman.marklar.core.helpers.DecimalHelper;
import cz.jlochman.marklar.core.helpers.TimeUtils;
import lombok.Data;
import lombok.extern.log4j.Log4j;

@Data
@Log4j
public class VlakVisDataRow {

   private final static String SEPARATOR         = ";";
   private final static int    TEMP_DEC_POINTS   = 1;
   private final static int    LENGTH_DEC_POINTS = 3;
   private final static int    SPEED_DEC_POINTS  = 3;
   private final static int    VALUES_COUNT      = 4;

   private long   time;
   private double temp;
   private double lengthMm;
   private double speedMmMin;
   private double speedLogMmMin;

   public VlakVisDataRow() {
   }

   public VlakVisDataRow(long time) {
      this.time = time;
   }

   public String asString() {
      StringBuilder sb = new StringBuilder();
      sb.append(getTempString())
        .append(SEPARATOR)
        .append(getLengthMmString())
        .append(SEPARATOR)
        .append(getSpeedMmMinString())
        .append(SEPARATOR)
        .append(getSpeedLogMmMinString())
        .append(SEPARATOR);
      return sb.toString();
   }

   public static VlakVisDataRow fromString(String s, long time) {
      VlakVisDataRow dataRow = new VlakVisDataRow(time);

      String[] items = s.split(SEPARATOR);
      if (items.length != VALUES_COUNT) {
         log.warn("bad num of values from string " + s + " expected: "
                  + VALUES_COUNT);
         return dataRow;
      }

      dataRow.setTemp(Double.parseDouble(items[0]));
      dataRow.setLengthMm(Double.parseDouble(items[1]));
      dataRow.setSpeedMmMin(Double.parseDouble(items[2]));
      dataRow.setSpeedLogMmMin(Double.parseDouble(items[3]));
      return dataRow;
   }

   public String getTimeString() {
      return TimeUtils.getStringFromLongTime(time);
   }

   public String getTempString() {
      return DecimalHelper.toString(temp, TEMP_DEC_POINTS);
   }

   public String getLengthMmString() {
      return DecimalHelper.toString(lengthMm, LENGTH_DEC_POINTS);
   }

   public String getSpeedMmMinString() {
      return DecimalHelper.toString(speedMmMin, SPEED_DEC_POINTS);
   }

   public String getSpeedLogMmMinString() {
      return DecimalHelper.toString(speedLogMmMin, SPEED_DEC_POINTS);
   }

}
