package cz.jlochman.marklar.vlak_vis;

import cz.jlochman.marklar.core.configuration.configurationFile.ConfigFileService;

public class VlakVisConfigIni extends ConfigFileService {

   private static final String NANODAC_IP = "nanodacIP";

   public String getNanodacIP() {
      return getValueForKey(NANODAC_IP);
   }

}
