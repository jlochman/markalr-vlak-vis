package cz.jlochman.marklar.vlak_vis;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import cz.jlochman.marklar.core.configuration.initializer.ConfigurationInitializer;
import cz.jlochman.marklar.core.configuration.initializer.MarklarMenuItem;
import cz.jlochman.marklar.core.helpers.ConnectionUtils;
import cz.jlochman.marklar.core.instruments.Instrument;
import cz.jlochman.marklar.core.language.I18n;
import cz.jlochman.marklar.drivers.connection.modbustcp.ModBusTcpConnection;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrument;
import lombok.extern.log4j.Log4j;

@Log4j
public class VlakVisInitializer implements ConfigurationInitializer {

   private final static int NUM_TRIES = 3;

   @Override
   public List<Instrument> initInstruments() {
      VlakVisConfigIni iniConfig = getIniConfig();

      List<Instrument> instruments = new ArrayList<>();

      VlakVisInstrument vlakVis =
            new VlakVisInstrument(0L, iniConfig.getNanodacIP());
      vlakVis.setName("Vláknový Viskozimetr");
      vlakVis.setDescription("Vláknový Viskozimetr");

      InetAddress addr = null;
      try {
         addr = InetAddress.getByName(iniConfig.getNanodacIP());
      } catch (UnknownHostException e) {
         e.printStackTrace();
      }
      ModBusTcpConnection eurothermConnection =
            new ModBusTcpConnection(addr, 1, vlakVis.getNanodac());
      vlakVis.getNanodac()
             .setConnection(eurothermConnection);

      try {
         if (ConnectionUtils.isReachable(iniConfig.getNanodacIP())) {
            vlakVis.connect();
            vlakVis.setTempSetpoint(400, NUM_TRIES);
            vlakVis.setOutHighLimit(80, NUM_TRIES);
            vlakVis.setOutLowLimit(0, NUM_TRIES);
         }
      } catch (Exception e) {
         log.error("nanodac unreachable on " + iniConfig.getNanodacIP());
      }

      instruments.add(vlakVis);
      return instruments;
   }

   @Override
   public void initLanguage() {
      I18n.getGraphicsBundle()
          .addBundle(ResourceBundle.getBundle("cz.jlochman.marklar.vlak_vis.language.graphics"));
      I18n.getMsgBundle()
          .addBundle(ResourceBundle.getBundle("cz.jlochman.marklar.vlak_vis.language.msg"));
   }

   @Override
   public VlakVisConfigIni getIniConfig() {
      return new VlakVisConfigIni();
   }

   @Override
   public List<MarklarMenuItem> getCustomMenuItems() {
      List<MarklarMenuItem> list = new ArrayList<>();

      // GraphicControllerDTO gcDTO =
      // AnchorPaneHelper.loadFXPaneByClass(CaliberSelectionController.class);
      // list.add(new MarklarMenuItem("Volba kalibru...", gcDTO));

      return list;
   }

   @Override
   public String getVersion() {
      return "VLAK_VIS_1.0";
   }

}
