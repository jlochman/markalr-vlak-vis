package cz.jlochman.marklar.vlak_vis.instrument;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cz.jlochman.marklar.core.instruments.Instrument;
import cz.jlochman.marklar.core.logicModules.settingsServices.InstrumentSettingsService;
import cz.jlochman.marklar.core.logicModules.settingsServices.exceptions.ValueNotFoundException;
import cz.jlochman.marklar.vlak_vis.graphics.settings.CalibreEntry;
import cz.jlochman.marklar.vlak_vis.graphics.settings.PowerRestriction;
import cz.jlochman.marklar.vlak_vis.graphics.settings.VlakVisCalibration;
import lombok.extern.log4j.Log4j;

@Log4j
public class VlakVisInstrumentSettingsService
      extends InstrumentSettingsService {

   private final static String TEMP_CORR = "tempCorr";

   private final static String CALIBRES = "calibres";

   private final static String RESTRICTION = "restriction";

   public VlakVisInstrumentSettingsService(Instrument instrument) {
      super(instrument);
   }

   public String getTempCorrectioner() {
      try {
         return loadStringValue(TEMP_CORR);
      } catch (ValueNotFoundException e) {
         return "";
      }
   }

   public void saveTempCorrectioner(String correctioner) {
      saveStringValue(correctioner, TEMP_CORR);
   }

   public PowerRestriction getPowerRestriction() {
      try {
         return PowerRestriction.fromString(loadStringValue(RESTRICTION));
      } catch (ValueNotFoundException e) {
         return new PowerRestriction(new ArrayList<>());
      }
   }

   public void savePowerRestriction(PowerRestriction restriction) {
      saveStringValue(restriction.asString(), RESTRICTION);
   }

   public static final String CALIBER_SEP = "@";

   public List<CalibreEntry> getCalibres() {
      try {
         return parseCalibres(loadStringValue(CALIBRES));
      } catch (Exception e) {
         return new ArrayList<>();
      }
   }

   public void saveCalibres(List<CalibreEntry> calibres) {
      String s = calibresToString(calibres);
      if (s != null && s.length() > 400) {
         log.warn("calibre string starts to be too big: " + s);
      }
      saveStringValue(s, CALIBRES);
      VlakVisCalibration.putCalibres(getObjectID(), calibres);
   }

   private List<CalibreEntry> parseCalibres(String calibresString) {
      ArrayList<CalibreEntry> list = new ArrayList<>();
      if (calibresString == null) {
         return list;
      }
      for (String s : calibresString.split(CALIBER_SEP)) {
         CalibreEntry sc = CalibreEntry.fromString(s);
         if (sc != null) {
            list.add(sc);
         }
      }
      return list;
   }

   private String calibresToString(List<CalibreEntry> calibres) {
      if (calibres == null) {
         return "";
      }
      return calibres.stream()
                     .map(CalibreEntry::asString)
                     .collect(Collectors.joining(CALIBER_SEP));
   }

}
