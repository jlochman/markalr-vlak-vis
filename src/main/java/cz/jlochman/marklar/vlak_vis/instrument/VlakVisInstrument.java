package cz.jlochman.marklar.vlak_vis.instrument;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantLock;

import cz.jlochman.marklar.core.ServiceLocator;
import cz.jlochman.marklar.core.components.DoubleValueIntAddrComponent;
import cz.jlochman.marklar.core.components.abstractComponents.Component;
import cz.jlochman.marklar.core.components.valueChecker.conditions.IntervalCondition;
import cz.jlochman.marklar.core.components.valueChecker.exceptions.GetValueException;
import cz.jlochman.marklar.core.components.valueChecker.exceptions.SetValueException;
import cz.jlochman.marklar.core.helpers.ConnectionUtils;
import cz.jlochman.marklar.core.helpers.Correctioner;
import cz.jlochman.marklar.core.instruments.Instrument;
import cz.jlochman.marklar.core.logicModules.observer.ExperimentLogicListener;
import cz.jlochman.marklar.core.logicModules.observer.LogicModuleEvent;
import cz.jlochman.marklar.vlak_vis.experiment.VlakVisExperimentType;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Log4j
public class VlakVisInstrument extends Instrument {

   @Getter
   private Nanodac nanodac;

   private DoubleValueIntAddrComponent thermometer;
   private DoubleValueIntAddrComponent tempSetPoint;

   private DoubleValueIntAddrComponent outHighLimit;
   private DoubleValueIntAddrComponent outLowLimit;

   private DoubleValueIntAddrComponent lengthMeter;

   private LengthHistory lengthHistory = new LengthHistory();

   private final ReentrantLock lock = new ReentrantLock(true);

   private final static long SLEEP_AFTER_READ_MILLIS  = 200L;
   private final static long SLEEP_AFTER_WRITE_MILLIS = 500L;
   private final static long SLEEP_AFTER_FAIL_MILLIS  = 1000L;

   public VlakVisInstrument(long id, String nanodacIp) {
      super(id);
      getSettingsService();
      setDefaultExpType(new VlakVisExperimentType());

      ServiceLocator.getInstance()
                    .getLogicModuleManager()
                    .registerListener(this, new ExperimentLogicListener() {

                       @Override
                       public void processEvent(LogicModuleEvent event) {
                          if (event instanceof TempCorrectionerEvent) {
                             if (thermometer != null) {
                                TempCorrectionerEvent pevent =
                                      (TempCorrectionerEvent) event;
                                thermometer.setCorrectioner(new Correctioner(pevent.getTempCorrectioner()));
                             }
                          }
                       }
                    });

      initNanodac(nanodacIp);
   }

   public boolean connect() {
      try {
         if (ConnectionUtils.isReachable(nanodac.getIpAddr())) {
            nanodac.connect();
         }
         return true;
      } catch (Exception e) {
         log.error("connection failed", e);
         return false;
      }
   }

   public double getTemperature(int numTries) throws IOException {
      if (numTries <= 0) {
         throw new IOException("temperature not obtained");
      }
      try {
         return getTemperature();
      } catch (Exception e) {
         log.warn(e.getMessage());
      }
      try {
         Thread.sleep(SLEEP_AFTER_FAIL_MILLIS);
      } catch (InterruptedException e) {
         Thread.currentThread()
               .interrupt();
      }
      connect();
      return getTemperature(numTries - 1);
   }

   private synchronized double getTemperature()
         throws IOException, GetValueException, Exception {
      double temp = 0;
      lock.lock();
      long start = System.currentTimeMillis();
      try {
         temp = thermometer.readValue(true);
      } catch (Exception e) {
         log.warn("exception getting temp");
      } finally {
         long toSleep =
               SLEEP_AFTER_READ_MILLIS - (System.currentTimeMillis() - start);
         if (toSleep > 0) {
            Thread.sleep(toSleep);
         }
         lock.unlock();
      }
      return temp;
   }

   public double getTempSetpoint(int numTries) throws IOException {
      if (numTries <= 0) {
         throw new IOException("tempSetpoint not obtained");
      }
      try {
         return getTempSetpoint();
      } catch (Exception e) {
         log.warn(e.getMessage());
      }
      try {
         Thread.sleep(SLEEP_AFTER_FAIL_MILLIS);
      } catch (InterruptedException e) {
         Thread.currentThread()
               .interrupt();
      }
      connect();
      return getTempSetpoint(numTries - 1);
   }

   private synchronized double getTempSetpoint()
         throws IOException, GetValueException, Exception {
      double tempSP = 0;
      lock.lock();
      long start = System.currentTimeMillis();
      try {
         tempSP = tempSetPoint.readValue(false);
      } catch (Exception e) {
         log.warn("exception getting tempSetpoint");
      } finally {
         long toSleep =
               SLEEP_AFTER_READ_MILLIS - (System.currentTimeMillis() - start);
         if (toSleep > 0) {
            Thread.sleep(toSleep);
         }
         lock.unlock();
      }
      return tempSP;
   }

   public void setTempSetpoint(double setPoint, int numTries)
         throws IOException {
      if (numTries <= 0) {
         throw new IOException("tempSetPoint not written");
      }
      try {
         setTempSetpoint(setPoint);
         return;
      } catch (Exception e) {
         log.warn(e.getMessage());
      }
      try {
         Thread.sleep(SLEEP_AFTER_FAIL_MILLIS);
      } catch (InterruptedException e) {
         Thread.currentThread()
               .interrupt();
      }
      connect();
      setTempSetpoint(setPoint, numTries - 1);
   }

   private synchronized void setTempSetpoint(double setPoint)
         throws IOException, SetValueException, Exception {
      lock.lock();
      long start = System.currentTimeMillis();
      try {
         tempSetPoint.writeValue(setPoint);

      } catch (Exception e) {
         log.warn("exception setting temp setpoint");
      } finally {
         long toSleep =
               SLEEP_AFTER_WRITE_MILLIS - (System.currentTimeMillis() - start);
         if (toSleep > 0) {
            Thread.sleep(toSleep);
         }
         lock.unlock();
      }
   }

   public void setOutLowLimit(double lowLimit, int numTries)
         throws IOException {
      if (numTries <= 0) {
         throw new IOException("outLowLimit not written");
      }
      try {
         setOutLowLimit(lowLimit);
         return;
      } catch (Exception e) {
         log.warn(e.getMessage());
      }
      try {
         Thread.sleep(SLEEP_AFTER_FAIL_MILLIS);
      } catch (InterruptedException e) {
         Thread.currentThread()
               .interrupt();
      }
      connect();
      setOutLowLimit(lowLimit, numTries - 1);
   }

   private synchronized void setOutLowLimit(double lowLimit)
         throws IOException, SetValueException, Exception {
      lock.lock();
      long start = System.currentTimeMillis();
      try {
         outLowLimit.writeValue(lowLimit);
      } catch (Exception e) {
         log.warn("exception setting outLowLimit");
      } finally {
         long toSleep =
               SLEEP_AFTER_WRITE_MILLIS - (System.currentTimeMillis() - start);
         if (toSleep > 0) {
            Thread.sleep(toSleep);
         }
         lock.unlock();
      }
   }

   public void setOutHighLimit(double highLimit, int numTries)
         throws IOException {
      if (numTries <= 0) {
         throw new IOException("outHighLimit not written");
      }
      try {
         setOutHighLimit(highLimit);
         return;
      } catch (Exception e) {
         log.warn(e.getMessage());
      }
      try {
         Thread.sleep(SLEEP_AFTER_FAIL_MILLIS);
      } catch (InterruptedException e) {
         Thread.currentThread()
               .interrupt();
      }
      connect();
      setOutHighLimit(highLimit, numTries - 1);
   }

   private synchronized void setOutHighLimit(double highLimit)
         throws IOException, SetValueException, Exception {
      lock.lock();
      long start = System.currentTimeMillis();
      try {
         outHighLimit.writeValue(highLimit);
      } catch (Exception e) {
         log.warn("exception setting outHighLimit");
      } finally {
         long toSleep =
               SLEEP_AFTER_WRITE_MILLIS - (System.currentTimeMillis() - start);
         if (toSleep > 0) {
            Thread.sleep(toSleep);
         }
         lock.unlock();
      }
   }

   public double getLength(int numTries) throws IOException {
      if (numTries <= 0) {
         throw new IOException("length not obtained");
      }
      try {
         return getLength();
      } catch (Exception e) {
         log.warn(e.getMessage());
      }
      try {
         Thread.sleep(SLEEP_AFTER_FAIL_MILLIS);
      } catch (InterruptedException e) {
         Thread.currentThread()
               .interrupt();
      }
      connect();
      return getLength(numTries - 1);
   }

   private synchronized double getLength()
         throws IOException, GetValueException, Exception {
      double length = 0;
      lock.lock();
      long start = System.currentTimeMillis();
      try {
         length = lengthMeter.readValue(false);
      } catch (Exception e) {
         log.warn("exception reading length");
      } finally {
         long toSleep =
               SLEEP_AFTER_READ_MILLIS - (System.currentTimeMillis() - start);
         if (toSleep > 0) {
            Thread.sleep(toSleep);
         }
         lock.unlock();
      }
      lengthHistory.addPoint(System.currentTimeMillis(), length);
      return length;
   }

   /**
    * Nejprve je treba zavolate {@link #getLength(boolean)}
    * 
    * @param correct
    *           zda se ma delka korigovat
    * @return rychlost v mm/min
    */
   public double getSpeed() {
      return lengthHistory.getSpeed();
   }

   private void initNanodac(String nanodacIp) {
      nanodac = new Nanodac(nanodacIp);
      List<Component> components = new ArrayList<>();

      thermometer = new DoubleValueIntAddrComponent(nanodac, 256, 10);
      thermometer.addGetCondition(new IntervalCondition(0, 1000));
      thermometer.setCorrectioner(new Correctioner(instrumentSS.getTempCorrectioner()));
      thermometer.setName("Channel.1.Main.PV");
      thermometer.setDescription("corrected temperature [˚C]");
      components.add(thermometer);

      tempSetPoint = new DoubleValueIntAddrComponent(nanodac, 5728, 10);
      tempSetPoint.addGetCondition(new IntervalCondition(0, 1000));
      tempSetPoint.addSetCondition(new IntervalCondition(0, 1000));
      tempSetPoint.setName("Loop.1.SP.AltSP [˚C]");
      tempSetPoint.setDescription("temp setPoint [˚C]");
      components.add(tempSetPoint);

      outHighLimit = new DoubleValueIntAddrComponent(nanodac, 5741, 10);
      outHighLimit.addSetCondition(new IntervalCondition(0, 100));
      outHighLimit.addGetCondition(new IntervalCondition(0, 100));
      outHighLimit.setName("Loop.1.OP.OutputHighLimit [%]");
      outHighLimit.setDescription("max output [%]");
      components.add(outHighLimit);

      outLowLimit = new DoubleValueIntAddrComponent(nanodac, 5742, 10);
      outLowLimit.addSetCondition(new IntervalCondition(0, 100));
      outLowLimit.addGetCondition(new IntervalCondition(0, 100));
      outLowLimit.setName("Loop.1.OP.OutputLowLimit [%]");
      outLowLimit.setDescription("min output [%]");
      components.add(outLowLimit);

      lengthMeter = new DoubleValueIntAddrComponent(nanodac, 264, 1000);
      lengthMeter.addGetCondition(new IntervalCondition(-100, 100));
      lengthMeter.setName("Channel.3.Main.PV");
      lengthMeter.setDescription("length [mm]");
      components.add(lengthMeter);

      nanodac.setComponents(components);
      nanodac.setName("Nanodac");
      nanodac.setDescription("Nanodac");
      this.addDevice(nanodac);
   }

   private VlakVisInstrumentSettingsService instrumentSS = null;

   @Override
   public VlakVisInstrumentSettingsService getSettingsService() {
      if (instrumentSS == null) {
         return instrumentSS = new VlakVisInstrumentSettingsService(this);
      }
      return instrumentSS;
   }

   @Override
   public void terminateProcedure() {
      try {
         if (ConnectionUtils.isReachable(nanodac.getIpAddr())) {
            nanodac.connect();
            setTempSetpoint(0, 3);
            setOutLowLimit(0, 3);
            setOutHighLimit(80, 3);
            nanodac.disconnect();
         }
      } catch (Exception e) {
         log.error("error terminating", e);
      }
   }

   private class LengthHistory {

      private volatile ConcurrentMap<Long, Double> lengthMap =
            new ConcurrentHashMap<>();

      private final static long KEEP_HISTORY_MILLIS = 30_000L;

      public synchronized void addPoint(long time, double value) {
         synchronized (lengthMap) {
            lengthMap.put(time, value);
            for (long pastTime : lengthMap.keySet()) {
               if (time - pastTime > KEEP_HISTORY_MILLIS) {
                  lengthMap.remove(pastTime);
               }
            }
         }
      }

      public synchronized double getSpeed() {
         synchronized (lengthMap) {
            if (lengthMap.size() <= 1) {
               return 0;
            }
            LongSummaryStatistics stats = lengthMap.keySet()
                                                   .stream()
                                                   .mapToLong(Long::longValue)
                                                   .summaryStatistics();
            long intevalMillis = stats.getMax() - stats.getMin();
            if (intevalMillis == 0) {
               return 0;
            }
            double lengthDiff =
                  lengthMap.get(stats.getMax()) - lengthMap.get(stats.getMin());
            if (Math.abs(lengthDiff) < 1e-10) {
               return 0;
            } else {
               return lengthDiff / ((double) intevalMillis / 1000 / 60);
            }
         }
      }

   }

}
