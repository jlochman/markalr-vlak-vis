package cz.jlochman.marklar.vlak_vis.instrument;

import cz.jlochman.marklar.core.logicModules.observer.LogicModuleEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class TempCorrectionerEvent extends LogicModuleEvent {

   private String tempCorrectioner;

}
