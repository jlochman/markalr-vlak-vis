package cz.jlochman.marklar.vlak_vis.instrument;

import java.net.ConnectException;

import cz.jlochman.marklar.core.devices.deviceEnums.DeviceStatus;
import cz.jlochman.marklar.core.devices.deviceInterfaces.IntAddrDevice;
import lombok.Getter;

public class Nanodac extends IntAddrDevice {

   @Getter
   private String ipAddr;

   public Nanodac(String ipAddr) {
      this.ipAddr = ipAddr;
   }

   @Override
   public void connect() throws ConnectException, Exception {
      connection.connect();
      deviceStatus = DeviceStatus.CONNECTED;
   }

   @Override
   public void disconnect() throws IllegalStateException, Exception {
      connection.disconnect();
      deviceStatus = DeviceStatus.DISCONNECTED;
   }

   @Override
   public String getInfo() {
      // TODO Auto-generated method stub
      return null;
   }

}
