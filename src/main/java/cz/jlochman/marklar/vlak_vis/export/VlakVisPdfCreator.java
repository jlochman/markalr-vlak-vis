package cz.jlochman.marklar.vlak_vis.export;

import java.awt.Color;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.encoding.WinAnsiEncoding;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import cz.jlochman.marklar.core.calculator.StatisticCalculator;
import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.entityDomain.Sample;
import cz.jlochman.marklar.core.helpers.DecimalHelper;
import cz.jlochman.marklar.core.helpers.TimeUtils;
import cz.jlochman.marklar.fxcommon.protocol.MyFont;
import cz.jlochman.marklar.fxcommon.protocol.MyPage;
import cz.jlochman.marklar.fxcommon.protocol.MyTable;
import cz.jlochman.marklar.fxcommon.protocol.MyText;
import cz.jlochman.marklar.fxcommon.protocol.TblRow;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;
import cz.jlochman.marklar.vlak_vis.experiment.settings.VlakVisExpTypeSettingsService;
import cz.jlochman.marklar.vlak_vis.graphics.settings.CalibreEntry;
import cz.jlochman.marklar.vlak_vis.graphics.settings.VlakVisCalibration;

public class VlakVisPdfCreator {

   private List<ExperimentInstance> expInstances;
   private String                   fullFileName;

   private VlakVisExpTypeSettingsService expTypeSS;
   private Sample                        sample;

   private float pageWidth;
   private float pageHeight;
   private float vertPos;

   public VlakVisPdfCreator(List<ExperimentInstance> expInstances,
                            String fullFileName) {
      this.expInstances = expInstances;
      this.fullFileName = fullFileName;

      if (expInstances != null && expInstances.size() > 0) {
         expTypeSS = (VlakVisExpTypeSettingsService) expInstances.get(0)
                                                                 .getExperimentType()
                                                                 .getExpDefaultSettingsService();
         sample = expInstances.get(0)
                              .getSample();
      }
   }

   public void createPdf() throws IOException {
      PDDocument document = new PDDocument();
      createFirstPage(document);
      for (int i = 0; i < expInstances.size(); i++) {
         createExperimentPage(document, expInstances.get(i), i + 1);
      }

      String pdfFileName = fullFileName + ".pdf";
      document.save(pdfFileName);
      document.close();

      Desktop.getDesktop()
             .open(new File(pdfFileName));
   }

   private void createFirstPage(PDDocument document) throws IOException {
      PDPage page = new PDPage();
      pageWidth = page.getMediaBox()
                      .getWidth();
      pageHeight = page.getMediaBox()
                       .getHeight();

      document.addPage(page);
      PDPageContentStream stream = new PDPageContentStream(document, page);

      // ==========================
      // ======= LOGO
      // ==========================
      //
      // File f = new File("logo.png");
      // if (f.exists()) {
      // PDImageXObject pdImage =
      // PDImageXObject.createFromFile(f.getAbsolutePath(), document);
      // stream.drawImage(pdImage, 420, 650);
      // }

      vertPos = MyPage.MARGIN_TOP;
      printTitle(stream, expTypeSS.getProtocolTitle());

      vertPos += 20;
      printSubTitle(stream, expTypeSS.getProtocolSubTitle());

      vertPos += 30;
      printTwoColls(stream, "Cislo vzorku", Long.toString(sample.getId()));
      vertPos += 20;
      printTwoColls(stream, "Nazev vzorku", sample.getName());
      vertPos += 20;
      printTwoColls(stream, "Kalibr",
                    getCalibreDescription(expInstances.get(0)));
      vertPos += 20;
      printTwoColls(stream, "Operator", expInstances.get(0)
                                                    .getOperator());
      vertPos += 20;
      printTwoColls(stream, "Datum",
                    TimeUtils.getStringFromDate(sample.getCreated(),
                                                "dd.MM. yyyy"));
      vertPos += 20;
      printTwoColls(stream, "Vysledky", "");
      vertPos += 30;

      TblRow tblRow = new TblRow(4);
      tblRow.add("#", "DCHT [˚C]", "HCHT [˚C]", "TRT [˚C]");
      printTblRow(stream, tblRow);
      printTblUnderscore(stream);

      List<Double> dchts = new ArrayList<>();
      List<Double> hchts = new ArrayList<>();
      List<Double> trts = new ArrayList<>();
      for (int i = 0; i < expInstances.size(); i++) {
         ExperimentInstance expInstance = expInstances.get(i);
         VlakVisDataService dataService =
               (VlakVisDataService) expInstance.getDataService();
         tblRow.clear();

         tblRow.add(Integer.toString(i + 1));

         double dcht = dataService.loadDcht();
         dchts.add(dcht);
         tblRow.add(DecimalHelper.toString(dcht, 1));

         double hcht = dataService.loadHcht();
         hchts.add(hcht);
         tblRow.add(DecimalHelper.toString(hcht, 1));
         
         double trt = dataService.loadTrt();
         trts.add(trt);
         tblRow.add(DecimalHelper.toString(trt, 1));

         vertPos += 20;
         printTblRow(stream, tblRow);
      }
      printTblUnderscore(stream);

      if (expInstances.size() > 1) {
         vertPos += 20;
         tblRow.clear();
         tblRow.add("");
         tblRow.add(StatisticCalculator.getStatistic(dchts)
                                       .asString(1, 1));
         tblRow.add(StatisticCalculator.getStatistic(hchts)
                    .asString(1, 1));
         tblRow.add(StatisticCalculator.getStatistic(trts)
                    .asString(1, 1));
         printTblRow(stream, tblRow, MyFont.FONT_BOLD);
      }

      vertPos += 30;
      printTwoColls(stream, "Popis vzorku", "");

      vertPos += 20;
      printText(stream, sample.getDescription());

      printFooter(stream);
      stream.close();
   }

   private final static double EPS = 1e-10;

   private String getCalibreDescription(ExperimentInstance expInstance) {
      VlakVisDataService dataService =
            (VlakVisDataService) expInstance.getDataService();
      double kki = dataService.loadKki();
      double ksi = dataService.loadKsi();

      long instrumentId = expInstance.getInstrument()
                                     .getId();
      List<CalibreEntry> calibres =
            VlakVisCalibration.getCalibres(instrumentId);

      CalibreEntry selectedCalibre = null;
      for (CalibreEntry calibre : calibres) {
         if (Math.abs(calibre.getKki() - kki) < EPS
             && Math.abs(calibre.getKsi() - ksi) < EPS) {
            selectedCalibre = calibre;
            break;
         }
      }
      if (selectedCalibre != null) {
         return selectedCalibre.getName();
      } else {
         return String.format("kki: %.2f  ksi: %.2f", kki, ksi);
      }
   }

   private void createExperimentPage(PDDocument document,
                                     ExperimentInstance expInstance,
                                     int counter)
         throws IOException {
      PDPage page = new PDPage();
      pageWidth = page.getMediaBox()
                      .getWidth();
      pageHeight = page.getMediaBox()
                       .getHeight();

      document.addPage(page);
      PDPageContentStream stream = new PDPageContentStream(document, page);

      VlakVisDataService dataService =
            (VlakVisDataService) expInstance.getDataService();
      vertPos = MyPage.MARGIN_TOP;
      printTwoColls(stream, "Cislo Mereni", Integer.toString(counter));
      vertPos += 20;
      printTwoColls(stream, "DCHT",
                    DecimalHelper.toString(dataService.loadDcht(), 1) + " ˚C");
      vertPos += 20;
      printTwoColls(stream, "HCHT",
                    DecimalHelper.toString(dataService.loadHcht(), 1) + " ˚C");
      vertPos += 20;
      printTwoColls(stream, "TRT",
                    DecimalHelper.toString(dataService.loadTrt(), 1) + " ˚C");

      printFooter(stream);
      stream.close();

      String imgName = fullFileName + "_" + counter + ".png";
      VlakVisImageService imgService = new VlakVisImageService();
      imgService.create(dataService, imgName);

      PDImageXObject pdImage = PDImageXObject.createFromFile(imgName, document);
      PDPageContentStream contentStream =
            new PDPageContentStream(document, page, AppendMode.APPEND, true);

      float scale = (pageWidth - MyPage.MARGIN_LEFT - MyPage.MARGIN_RIGHT)
                    / pdImage.getWidth();
      contentStream.drawImage(pdImage, MyPage.MARGIN_LEFT,
                              (pageHeight - pdImage.getHeight() * scale) / 2,
                              pdImage.getWidth() * scale,
                              pdImage.getHeight() * scale);

      contentStream.close();
   }

   @SuppressWarnings("deprecation")
   private void printFooter(PDPageContentStream stream) throws IOException {
      PDFont font = MyFont.FONT;
      float fontSize = MyFont.SIZE_SMALL;

      stream.beginText();
      stream.setFont(font, fontSize);
      stream.moveTextPositionByAmount(MyPage.MARGIN_LEFT, MyPage.MARGIN_BOTTOM);
      stream.drawString("podle normy ASTM C 336 - 71");
      stream.endText();
   }

   @SuppressWarnings("deprecation")
   private void printTitle(PDPageContentStream stream, String title)
         throws IOException {
      PDFont font = MyFont.FONT_BOLD;
      float fontSize = MyFont.SIZE_HUGE;
      float titleWidth = font.getStringWidth(title) / 1000 * fontSize;

      stream.beginText();
      stream.setFont(font, fontSize);
      stream.moveTextPositionByAmount((pageWidth - titleWidth) / 2,
                                      pageHeight - MyPage.MARGIN_TOP - vertPos);
      stream.drawString(normalizeString(title));
      stream.endText();
   }

   @SuppressWarnings("deprecation")
   private void printSubTitle(PDPageContentStream stream, String title)
         throws IOException {
      PDFont font = MyFont.FONT_BOLD;
      float fontSize = MyFont.SIZE_BIG;
      float titleWidth = font.getStringWidth(title) / 1000 * fontSize;
      // float titleHeight =
      // font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 *
      // fontSize;

      stream.beginText();
      stream.setFont(font, fontSize);
      stream.moveTextPositionByAmount((pageWidth - titleWidth) / 2,
                                      pageHeight - MyPage.MARGIN_TOP - vertPos);
      stream.drawString(normalizeString(title));
      stream.endText();
   }

   @SuppressWarnings("deprecation")
   private void printTwoColls(PDPageContentStream stream, String leftText,
                              String rightText)
         throws IOException {
      stream.beginText();
      stream.setFont(MyFont.FONT_BOLD, MyFont.SIZE_NORMAL);
      stream.moveTextPositionByAmount(MyPage.MARGIN_LEFT,
                                      pageHeight - MyPage.MARGIN_TOP - vertPos);
      stream.drawString(normalizeString(leftText));
      stream.setFont(MyFont.FONT, MyFont.SIZE_NORMAL);
      stream.moveTextPositionByAmount(pageWidth / 2 - MyPage.MARGIN_LEFT, 0);
      stream.drawString(normalizeString(rightText));
      stream.endText();
   }

   private void printTblRow(PDPageContentStream stream, TblRow tblRow)
         throws IOException {
      printTblRow(stream, tblRow, MyFont.FONT, Color.BLACK);
   }

   private void printTblRow(PDPageContentStream stream, TblRow tblRow,
                            PDFont myFont)
         throws IOException {
      printTblRow(stream, tblRow, myFont, Color.BLACK);
   }

   @SuppressWarnings("deprecation")
   private void printTblRow(PDPageContentStream stream, TblRow tblRow,
                            PDFont myFont, Color color)
         throws IOException {
      float colWidth = (pageWidth - MyTable.MARGIN_LEFT - MyTable.MARGIN_RIGHT)
                       / (tblRow.getSize() + 2);

      stream.beginText();
      stream.setNonStrokingColor(color);
      stream.setFont(myFont, MyFont.SIZE_NORMAL);
      stream.moveTextPositionByAmount(MyPage.MARGIN_LEFT + MyTable.MARGIN_RIGHT,
                                      pageHeight - MyPage.MARGIN_TOP - vertPos + 5);
      for (String s : tblRow.getList()) {
         stream.drawString(normalizeString(s));
         stream.moveTextPositionByAmount(colWidth, 0);
      }
      stream.endText();
      stream.setNonStrokingColor(Color.BLACK);
   }

   @SuppressWarnings("deprecation")
   private void printTblUnderscore(PDPageContentStream stream)
         throws IOException {
      stream.setStrokingColor(Color.BLACK);
      float tblLeft = MyPage.MARGIN_LEFT + MyTable.MARGIN_LEFT;
      float tblRight = pageWidth - MyTable.MARGIN_RIGHT - MyTable.MARGIN_RIGHT;
      float lineVertical = pageHeight - MyPage.MARGIN_TOP - vertPos - 2;
      stream.drawLine(tblLeft, lineVertical, tblRight, lineVertical);
   }

   @SuppressWarnings("deprecation")
   private void printText(PDPageContentStream stream, String text)
         throws IOException {
      List<String> lines =
            breakLongString(text, MyFont.FONT, MyFont.SIZE_NORMAL);

      stream.beginText();
      stream.setFont(MyFont.FONT, MyFont.SIZE_NORMAL);
      stream.moveTextPositionByAmount(MyPage.MARGIN_LEFT + MyText.MARGIN_LEFT,
                                      pageHeight - MyPage.MARGIN_TOP - vertPos);
      for (String s : lines) {
         stream.drawString(normalizeString(s));
         stream.moveTextPositionByAmount(0, -20);
         vertPos += 20;
      }

      stream.endText();
   }

   private List<String> breakLongString(String s, PDFont pdfFont,
                                        float fontSize)
         throws IOException {
      List<String> lines = new ArrayList<String>();

      if (s == null)
         return lines;

      s = normalizeString(s);

      float width = pageWidth - MyPage.MARGIN_LEFT - MyPage.MARGIN_RIGHT
                    - MyText.MARGIN_LEFT - MyText.MARGIN_RIGHT;
      int lastSpace = -1;

      while (s.length() > 0) {
         int spaceIndex = s.indexOf(' ', lastSpace + 1);
         if (spaceIndex < 0)
            spaceIndex = s.length();
         String subString = s.substring(0, spaceIndex);
         float size = fontSize * pdfFont.getStringWidth(subString) / 1000;
         if (size > width) {
            if (lastSpace < 0)
               lastSpace = spaceIndex;
            subString = s.substring(0, lastSpace);
            lines.add(subString);
            s = s.substring(lastSpace)
                 .trim();
            lastSpace = -1;
         } else if (spaceIndex == s.length()) {
            lines.add(s);
            s = "";
         } else {
            lastSpace = spaceIndex;
         }
      }

      return lines;
   }

   private String normalizeString(String s) {
      if (s == null)
         return "";
      if (s.isEmpty())
         return "";
      String resultString = Normalizer.normalize(s, Normalizer.Form.NFD);
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < resultString.length(); i++) {
         if (WinAnsiEncoding.INSTANCE.contains(resultString.charAt(i))) {
            sb.append(resultString.charAt(i));
         }
      }
      resultString = sb.toString();
      resultString = resultString.replace("\n", "")
                                 .replace("\r", "");
      return resultString;
   }

}
