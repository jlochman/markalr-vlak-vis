package cz.jlochman.marklar.vlak_vis.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.NotImplementedException;

import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.experiments.export.ExperimentExportService;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataRow;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;

public class VlakVisExportService extends ExperimentExportService {

   public VlakVisExportService(List<ExperimentInstance> expInstances) {
      super(expInstances);
   }

   @Override
   public void exportAsTXT(String fullFileName) throws IOException {
      throw new NotImplementedException("export as txt not implemented");
   }

   @Override
   public void exportAsCSV(String fullFileName) throws IOException {
      int counter = 0;
      for (ExperimentInstance expInstance : expInstances) {
         counter++;

         File file = new File(fullFileName + "_" + counter + ".csv");
         if (!file.exists()) {
            file.createNewFile();
         }

         FileWriter fw = new FileWriter(file.getAbsoluteFile());
         BufferedWriter bw = new BufferedWriter(fw);

         VlakVisDataService dataService =
               (VlakVisDataService) expInstance.getDataService();
         List<VlakVisDataRow> dataRows = dataService.getDataRows();

         String head =
               "Cas,Teplota,Delka[mm],Rychlost[mm/min],LogRychlost[log(mm/min)]";
         bw.write(head);
         bw.newLine();

         for (VlakVisDataRow dataRow : dataRows) {
            String line = dataRow.getTimeString();
            line += "," + dataRow.getTempString();
            line += "," + dataRow.getLengthMmString();
            line += "," + dataRow.getSpeedMmMinString();
            line += "," + dataRow.getSpeedLogMmMinString();
            bw.write(line);
            bw.newLine();
         }

         bw.close();
         fw.close();
      }

   }

   @Override
   public void exportAsPDF(String fullFileName) throws IOException {
      VlakVisPdfCreator pdfCreator =
            new VlakVisPdfCreator(expInstances, fullFileName);
      pdfCreator.createPdf();
   }

}
