package cz.jlochman.marklar.vlak_vis.export;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;
import cz.jlochman.marklar.vlak_vis.graphics.VlakVisChart;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.image.WritableImage;

public class VlakVisImageService {

   public void create(VlakVisDataService dataService, String imageName)
         throws IOException {
      VlakVisChart chart = new VlakVisChart(false);
      chart.repaint(dataService);
      LineChart<Number, Number> lineChart = chart.getChart();
      lineChart.getStylesheets()
               .clear();
      lineChart.getStylesheets()
               .addAll(getClass().getResource("chart-style.css")
                                 .toExternalForm());

      Scene scene = new Scene(lineChart, 1000, 700);
      WritableImage image = new WritableImage(1000, 700);
      scene.snapshot(image);

      File file = new File(imageName);
      RenderedImage renderedImage = SwingFXUtils.fromFXImage(image, null);
      ImageIO.write(renderedImage, "png", file);
   }

}
