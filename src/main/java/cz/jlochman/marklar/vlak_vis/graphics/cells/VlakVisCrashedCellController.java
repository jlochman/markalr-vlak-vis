package cz.jlochman.marklar.vlak_vis.graphics.cells;

import java.net.URL;
import java.util.ResourceBundle;

import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.graphicsDefinitions.GenericCell;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;
import cz.jlochman.marklar.vlak_vis.graphics.VlakVisChart;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

public class VlakVisCrashedCellController extends GenericCell {

   @FXML
   private AnchorPane chartPane;

   private VlakVisDataService dataService;

   public VlakVisCrashedCellController(ExperimentInstance expInst) {
      super(expInst);
      setFXMLFile("VlakVisCrashedCell.fxml");

      dataService = (VlakVisDataService) expInst.getDataService();
   }

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      // TODO Auto-generated method stub

   }

   @Override
   public void fillGraphics() {
      setBackgroundColor();

      VlakVisChart chart = new VlakVisChart(chartPane, true);
      chart.repaint(dataService);
   }

}
