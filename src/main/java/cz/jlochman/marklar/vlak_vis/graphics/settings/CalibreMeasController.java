package cz.jlochman.marklar.vlak_vis.graphics.settings;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import cz.jlochman.marklar.fxcommon.utils.DialogUtils;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrument;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.WindowEvent;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Log4j
public class CalibreMeasController implements Initializable {

   @FXML
   private TextField edtTemp, edtLength, edtDiameter, edtWeight;
   @FXML
   private Button    btnWriteTemp, btnStart, btnStop, btnSave;
   @FXML
   private Label     lblSpeed, lblViskozity;

   @FXML
   private LineChart<Double, Double> chart;
   @FXML
   private NumberAxis                xAxis;
   private ChartData                 chartData;

   private CalibrePaneController calibrePaneController;
   private VlakVisInstrument     instrument;

   private CalibreMeasThread thread = null;

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      btnWriteTemp.setOnAction(this::writeTempPressed);
      btnStart.setOnAction(this::startPressed);
      btnStop.setOnAction(this::stopPressed);
      btnSave.setOnAction(this::savePressed);

      lblSpeed.setText("");
      lblViskozity.setText("");

      xAxis.setAutoRanging(false);
      xAxis.setLowerBound(-ChartData.KEEP_HISTORY_MILLIS / 1000);
      xAxis.setUpperBound(0);
      xAxis.setTickUnit(60);

      chart.setLegendVisible(false);
      chart.setAnimated(false);
      chart.getYAxis()
           .setLabel("log viskozity [log(dPas)]");
   }

   public void
         setCalibrePaneController(CalibrePaneController calibrePaneController) {
      this.calibrePaneController = calibrePaneController;
   }

   public void setInstrument(VlakVisInstrument instrument) {
      this.instrument = instrument;
   }

   private void writeTempPressed(ActionEvent event) {
      try {
         double temp = Double.parseDouble(edtTemp.getText());
         instrument.setTempSetpoint(temp, 3);
      } catch (Exception e) {
         log.info("error writing temperature", e);
      }
   }

   private void savePressed(ActionEvent event) {
      try {
         double diameterMm = Double.parseDouble(edtDiameter.getText());
         double speedMmMin = Double.parseDouble(lblSpeed.getText());
         calibrePaneController.addData(diameterMm, speedMmMin);
      } catch (Exception e) {
         Alert alert =
               DialogUtils.buildSimpleDialog("Spatne hodnoty", "Spatne hodnoty",
                                             "Ziskane hodnoty prumeru a rychlosti jsou nespravne",
                                             AlertType.WARNING);
         alert.showAndWait();
      }
   }

   private void startPressed(ActionEvent event) {
      btnStart.getScene()
              .getWindow()
              .setOnCloseRequest(new EventHandler<WindowEvent>() {

                 @Override
                 public void handle(WindowEvent event) {
                    if (thread != null) {
                       thread.stopThread();
                    }
                 }
              });

      try {
         chartData = new ChartData();
         chartData.setParams(Double.parseDouble(edtLength.getText()),
                             Double.parseDouble(edtDiameter.getText()),
                             Double.parseDouble(edtWeight.getText()));
      } catch (Exception e) {
         Alert alert =
               DialogUtils.buildSimpleDialog("Spatne vstupy", "Spatne vstupy",
                                             "Zadane hodnoty pro delku, prumer nebo vahu se nepodarilo prevest na cislo.",
                                             AlertType.WARNING);
         alert.showAndWait();
         return;
      }

      thread = new CalibreMeasThread();
      thread.start();
      btnStart.setDisable(true);
   }

   private void stopPressed(ActionEvent event) {
      if (thread != null) {
         thread.stopThread();
         btnStart.setDisable(false);
      }
   }

   private void repaint(ChartData chartData) {
      chart.getData()
           .clear();
      chart.getData()
           .add(chartData.getSerie());
      lblSpeed.setText(String.format("%.3f", chartData.getLastSpeedMmMin()));
      lblViskozity.setText(String.format("%.3f", chartData.getLastViskozity()));
   }

   private class ChartData {

      private ConcurrentMap<Long, Double> dataMap = new ConcurrentHashMap<>();
      private long                        lastTime;

      private final static long KEEP_HISTORY_MILLIS = 300_000L;

      private final static double MIN_VISK = 1e-3;

      private double initLengthMm, initDiameterMm, weight;

      @Getter
      private double lastSpeedMmMin, lastViskozity;

      public void setParams(double initLengthMm, double initDiameterMm,
                            double weight) {
         this.initLengthMm = initLengthMm;
         this.initDiameterMm = initDiameterMm;
         this.weight = weight;
      }

      private double getViskosityLog(double speedMmMin) {
         if (initLengthMm == 0 || initDiameterMm == 0 || speedMmMin == 0) {
            return 0;
         }
         double visk = 32700 * weight * initLengthMm;
         visk /= Math.PI * Math.pow(initDiameterMm, 2) / 4;
         visk /= speedMmMin / 60;

         if (visk > MIN_VISK) {
            visk = Math.log10(visk);
         } else {
            visk = Math.log10(MIN_VISK);
         }

         lastViskozity = visk;
         return visk;
      }

      public void addPoint(long time, double speedMmMin) {
         lastTime = time;
         synchronized (dataMap) {
            lastSpeedMmMin = speedMmMin;
            dataMap.put(time, getViskosityLog(speedMmMin));
            for (long pastTime : dataMap.keySet()) {
               if (time - pastTime > KEEP_HISTORY_MILLIS) {
                  dataMap.remove(pastTime);
               }
            }
         }
      }

      public Series<Double, Double> getSerie() {
         Series<Double, Double> serie = new Series<>();
         synchronized (dataMap) {
            for (long time : dataMap.keySet()) {
               serie.getData()
                    .add(new Data<>(((double) (time - lastTime)) / 1000,
                                    dataMap.get(time)));
            }
         }
         return serie;
      }

   }

   private class CalibreMeasThread extends Thread {

      private volatile boolean stopped = false;

      private final static long MESS_INTERVAL = 2000L;
      private final static int  NUM_TRIES     = 3;

      @Override
      public void run() {
         if (!instrument.connect()) {
            return;
         }
         while (!stopped) {
            try {
               instrument.getLength(NUM_TRIES);
               chartData.addPoint(System.currentTimeMillis(),
                                  Math.abs(instrument.getSpeed()));

               Platform.runLater(() -> repaint(chartData));
               Thread.sleep(MESS_INTERVAL);
            } catch (InterruptedException e) {
               Thread.currentThread()
                     .interrupt();
            } catch (Exception e) {
               log.warn("error reading data from instrument", e);
            }
         }
      }

      public void stopThread() {
         stopped = true;
      }

   }

}
