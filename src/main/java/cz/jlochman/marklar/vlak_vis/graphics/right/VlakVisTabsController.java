package cz.jlochman.marklar.vlak_vis.graphics.right;

import java.net.URL;
import java.util.ResourceBundle;

import cz.jlochman.marklar.core.ServiceLocator;
import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.experiments.common.ExperimentStatus;
import cz.jlochman.marklar.core.graphicsDefinitions.AnchorPaneHelper;
import cz.jlochman.marklar.core.graphicsDefinitions.GraphicControllerDTO;
import cz.jlochman.marklar.core.logicModules.observer.ExperimentLogicListener;
import cz.jlochman.marklar.core.logicModules.observer.LogicModuleEvent;
import cz.jlochman.marklar.fxcommon.experiment.infoPane.ExperimentInfoPaneController;
import cz.jlochman.marklar.fxcommon.rightPane.ExperimentTabsController;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataRow;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;
import cz.jlochman.marklar.vlak_vis.experiment.NewDataEvent;
import cz.jlochman.marklar.vlak_vis.graphics.VlakVisChart;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;

public class VlakVisTabsController extends ExperimentTabsController {

   private static final int MAX_TAB_COUNT = 4;

   @FXML
   private AnchorPane mainPane, infoPane, chartPane, dataPane;

   private VlakVisDataService dataService;

   private ExperimentInfoPaneController infoTabController;
   private VlakVisChart                 chart;
   private VlakVisTableTabController    tableTabController;

   private ExperimentLogicListener instanceListener;

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      // TODO Auto-generated method stub

   }

   @Override
   public void setExpInstance(ExperimentInstance expInstance) {
      if (expInstance == null) {
         return;
      }
      if (this.expInstance != null
          && this.expInstance.getId() == expInstance.getId()) {
         return;
      }
      if (instanceListener != null) {
         ServiceLocator.getInstance()
                       .getLogicModuleManager()
                       .unregisterListener(this.expInstance, instanceListener);
      }
      super.setExpInstance(expInstance);

      dataService = (VlakVisDataService) expInstance.getDataService();

      GraphicControllerDTO gcDTO = getInfoTabGCDTO();
      infoTabController = (ExperimentInfoPaneController) gcDTO.getController();
      infoTabController.setExpInstance(expInstance);
      AnchorPaneHelper.insertNodeToContent(gcDTO.getGraphic(), infoPane);

      chart = new VlakVisChart(chartPane, false);

      gcDTO =
            AnchorPaneHelper.loadFXPaneByClass(VlakVisTableTabController.class);
      tableTabController = (VlakVisTableTabController) gcDTO.getController();
      tableTabController.setExpInstance(expInstance);
      AnchorPaneHelper.insertNodeToContent(gcDTO.getGraphic(), dataPane);

      showHideExportTab();

      instanceListener = new ExperimentLogicListener() {

         @Override
         public void processEvent(LogicModuleEvent event) {
            if (event instanceof NewDataEvent) {
               NewDataEvent pevent = (NewDataEvent) event;
               newData(pevent.getDataRow());
            }
         }
      };
      ServiceLocator.getInstance()
                    .getLogicModuleManager()
                    .registerListener(expInstance, instanceListener);
   }

   @Override
   public void unregisterListeners() {
      super.unregisterListeners();
      if (instanceListener != null) {
         ServiceLocator.getInstance()
                       .getLogicModuleManager()
                       .unregisterListener(this.expInstance, instanceListener);
      }
   }

   @Override
   public void clearGraphics() {
      mainPane.getChildren()
              .clear();
   }

   @Override
   public void repaint() {
      if (expInstance == null || dataService == null) {
         return;
      }

      infoTabController.repaint();
      chart.repaint(dataService);

      tableTabController.setExpInstance(expInstance);
      tableTabController.repaint();
   }

   @Override
   public void newData(Object dataRow) {
      if (!(dataRow instanceof VlakVisDataRow)) {
         return;
      }
      VlakVisDataRow dr = (VlakVisDataRow) dataRow;

      chart.addData(dr);
      tableTabController.newData(dataRow);
   }

   private void showHideExportTab() {
      if (expInstance.getStatus() == ExperimentStatus.ABORTED
          || expInstance.getStatus() == ExperimentStatus.ENDED
          || expInstance.getStatus() == ExperimentStatus.SUSPENDED) {
         if (tabPane.getTabs()
                    .size() == MAX_TAB_COUNT) {
            return;
         }
         Tab tab = new Tab("Export");
         GraphicControllerDTO gcDTO =
               AnchorPaneHelper.loadFXPaneByClass(VlakVisExportTabController.class);
         tab.setContent(gcDTO.getGraphic());
         VlakVisExportTabController controller =
               (VlakVisExportTabController) gcDTO.getController();
         controller.setExpInstance(expInstance);
         tabPane.getTabs()
                .add(tab);
      } else {
         if (tabPane.getTabs()
                    .size() == MAX_TAB_COUNT) {
            tabPane.getTabs()
                   .remove(MAX_TAB_COUNT - 1);
         }
      }
   }

}
