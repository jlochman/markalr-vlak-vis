package cz.jlochman.marklar.vlak_vis.graphics.cells;

import java.net.URL;
import java.util.ResourceBundle;

import cz.jlochman.marklar.core.ServiceLocator;
import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.graphicsDefinitions.GenericCell;
import cz.jlochman.marklar.core.helpers.DecimalHelper;
import cz.jlochman.marklar.core.logicModules.observer.ExperimentLogicListener;
import cz.jlochman.marklar.core.logicModules.observer.LogicModuleEvent;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;
import cz.jlochman.marklar.vlak_vis.graphics.VlakVisChart;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class VlakVisEndedCellController extends GenericCell {

   @FXML
   private Label      lblHcht, lblDcht, lblTrt;
   @FXML
   private AnchorPane chartPane;

   private VlakVisDataService dataService;

   private ExperimentLogicListener expInstnaceListener;

   public VlakVisEndedCellController(ExperimentInstance expInst) {
      super(expInst);
      setFXMLFile("VlakVisEndedCell.fxml");

      dataService = (VlakVisDataService) expInst.getDataService();

      expInstnaceListener = new ExperimentLogicListener() {

         @Override
         public void processEvent(LogicModuleEvent event) {
            if (event instanceof ResultTemperaturesChangedEvent) {
               ResultTemperaturesChangedEvent pevent =
                     (ResultTemperaturesChangedEvent) event;
               updateResultTemperatures(pevent.getHcht(), pevent.getDcht(),
                                        pevent.getTrt());
            }

         }
      };
      ServiceLocator.getInstance()
                    .getLogicModuleManager()
                    .registerListener(expInst, expInstnaceListener);
   }

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      // TODO Auto-generated method stub

   }

   @Override
   protected void finalize() throws Throwable {
      super.finalize();
      if (expInstnaceListener != null) {
         ServiceLocator.getInstance()
                       .getLogicModuleManager()
                       .unregisterListener(expInst, expInstnaceListener);
      }
   }

   @Override
   public void fillGraphics() {
      setBackgroundColor();
      updateResultTemperatures(dataService.loadHcht(), dataService.loadDcht(),
                               dataService.loadTrt());

      VlakVisChart chart = new VlakVisChart(chartPane, true);
      chart.repaint(dataService);
   }

   private void updateResultTemperatures(double hcht, double dcht, double trt) {
      if (lblHcht != null) {
         lblHcht.setText(DecimalHelper.toString(hcht, 1) + " ˚C");
      }
      if (lblDcht != null) {
         lblDcht.setText(DecimalHelper.toString(dcht, 1) + " ˚C");
      }
      if (lblTrt != null) {
         lblTrt.setText(DecimalHelper.toString(trt, 1) + " ˚C");
      }
   }

}
