package cz.jlochman.marklar.vlak_vis.graphics.settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.jlochman.marklar.core.ServiceLocator;
import cz.jlochman.marklar.core.instruments.Instrument;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrument;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrumentSettingsService;
import lombok.extern.log4j.Log4j;

@Log4j
public class VlakVisCalibration {

   private static Map<Long, List<CalibreEntry>> instrumentCalibres =
         new HashMap<>();

   public VlakVisCalibration() {
   }

   public static void putCalibres(long instrumentId,
                                  List<CalibreEntry> calibres) {
      synchronized (instrumentCalibres) {
         instrumentCalibres.put(instrumentId, calibres);
      }
   }

   public static List<CalibreEntry> getCalibres(long instrumentId) {
      synchronized (instrumentCalibres) {
         if (!instrumentCalibres.containsKey(instrumentId)) {
            Instrument instrument = ServiceLocator.getInstance()
                                                  .getInstrumentService()
                                                  .getInstrumentByID((int) instrumentId);
            if (instrument == null) {
               log.error("null instrument for id:" + instrumentId);
               return new ArrayList<>();
            }
            if (!(instrument instanceof VlakVisInstrument)) {
               log.error("instrument with id:" + instrumentId
                         + " is not vlakVis");
               return new ArrayList<>();
            }
            VlakVisInstrumentSettingsService instrumentSS =
                  ((VlakVisInstrument) instrument).getSettingsService();
            instrumentCalibres.put(instrumentId, instrumentSS.getCalibres());
         }
         return instrumentCalibres.get(instrumentId);
      }
   }

}
