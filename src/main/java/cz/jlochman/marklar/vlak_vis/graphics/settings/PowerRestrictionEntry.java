package cz.jlochman.marklar.vlak_vis.graphics.settings;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PowerRestrictionEntry {

   private int    tempFromIncl, tempToExcl;
   private int lowLimit, highLimit;

   public boolean isValid() {
      return isTempValid(tempFromIncl) && isTempValid(tempToExcl)
             && tempFromIncl < tempToExcl && isLimitValid(lowLimit)
             && isLimitValid(highLimit) && lowLimit < highLimit;
   }

   private boolean isTempValid(double temp) {
      return temp >= 0 && temp <= 1000;
   }

   private boolean isLimitValid(double limit) {
      return limit >= 0 && limit <= 100;
   }

   public double getTempHeuristics(double temp) {
      if (temp >= tempFromIncl && temp < tempToExcl) {
         return -1;
      }
      if (temp < tempFromIncl) {
         return tempFromIncl - temp;
      }
      if (temp >= tempToExcl) {
         return temp - tempToExcl;
      }
      return 0;
   }

}
