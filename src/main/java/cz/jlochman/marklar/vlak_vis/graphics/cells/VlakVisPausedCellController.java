package cz.jlochman.marklar.vlak_vis.graphics.cells;

import java.net.URL;
import java.util.ResourceBundle;

import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.graphicsDefinitions.GenericCell;

public class VlakVisPausedCellController extends GenericCell {

   public VlakVisPausedCellController(ExperimentInstance expInst) {
      super(expInst);
      setFXMLFile("VlakVisPausedCell.fxml");
   }

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      // TODO Auto-generated method stub

   }

   @Override
   public void fillGraphics() {
      setBackgroundColor();

   }

}
