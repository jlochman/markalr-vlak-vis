package cz.jlochman.marklar.vlak_vis.graphics.right;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.fxcommon.experiment.ExperimentInstanceController;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataRow;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.Data;

public class VlakVisTableTabController extends ExperimentInstanceController {

   @FXML
   private TableView<TableRow> dataTable;

   @FXML
   private TableColumn<TableRow, Long>   dataTime;
   @FXML
   private TableColumn<TableRow, String> dataTemp, dataLength, dataSpeed,
         dataSpeedLog;

   private VlakVisDataService dataService;

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      dataTime.setCellValueFactory(new PropertyValueFactory<TableRow,
                                                            Long>("time"));
      dataTemp.setCellValueFactory(new PropertyValueFactory<TableRow,
                                                            String>("temp"));
      dataLength.setCellValueFactory(new PropertyValueFactory<TableRow,
                                                              String>("length"));
      dataSpeed.setCellValueFactory(new PropertyValueFactory<TableRow,
                                                             String>("speed"));
      dataSpeedLog.setCellValueFactory(new PropertyValueFactory<TableRow,
                                                                String>("speedLog"));
   }

   @Override
   public void setExpInstance(ExperimentInstance expInstance) {
      super.setExpInstance(expInstance);

      dataService = (VlakVisDataService) expInstance.getDataService();
   }

   @Override
   public void repaint() {
      dataTable.getItems()
               .clear();
      List<VlakVisDataRow> dataRows = dataService.getDataRows();
      if (dataRows != null) {
         ObservableList<TableRow> data = FXCollections.observableArrayList();
         for (VlakVisDataRow dataRow : dataRows) {
            data.add(new TableRow(dataRow));
         }
         dataTable.setItems(data);
      }
   }

   @Override
   public void newData(Object dataRow) {
      if (!(dataRow instanceof VlakVisDataRow)) {
         return;
      }
      dataTable.getItems()
               .add(new TableRow((VlakVisDataRow) dataRow));
   }

   @Data
   public class TableRow {

      private SimpleStringProperty time;
      private SimpleStringProperty temp;
      private SimpleStringProperty length;
      private SimpleStringProperty speed;
      private SimpleStringProperty speedLog;

      public TableRow(VlakVisDataRow dataRow) {
         time = new SimpleStringProperty(dataRow.getTimeString());
         temp = new SimpleStringProperty(dataRow.getTempString());
         length = new SimpleStringProperty(dataRow.getLengthMmString());
         speed = new SimpleStringProperty(dataRow.getSpeedMmMinString());
         speedLog = new SimpleStringProperty(dataRow.getSpeedLogMmMinString());
      }

      public String getTime() {
         return time.getValue();
      }

      public String getTemp() {
         return temp.getValue();
      }

      public String getLength() {
         return length.getValue();
      }

      public String getSpeed() {
         return speed.getValue();
      }

      public String getSpeedLog() {
         return speedLog.getValue();
      }

   }

}
