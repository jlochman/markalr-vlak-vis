package cz.jlochman.marklar.vlak_vis.graphics.settings;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

import cz.jlochman.marklar.core.graphicsDefinitions.AnchorPaneHelper;
import cz.jlochman.marklar.core.graphicsDefinitions.GraphicControllerDTO;
import cz.jlochman.marklar.fxcommon.utils.ListViewUtils;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrument;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;

public class CalibrePaneController implements Initializable {

   @FXML
   private TextField                 edtKKi, edtKSi, edtName;
   @FXML
   private CheckBox                  cbKKiKSi;
   @FXML
   private ListView<DataPoint>       lwData;
   @FXML
   private LineChart<Double, Double> chart;
   @FXML
   private Button                    btnStart;

   private CalibreEntry      entry;
   private VlakVisInstrument instrument;

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      cbKKiKSi.selectedProperty().addListener(new ChangeListener<Boolean>() {

         @Override
         public void changed(ObservableValue<? extends Boolean> observable,
                             Boolean oldValue, Boolean newValue) {
            boolean selected = newValue;
            
            edtKKi.setDisable(selected);
            edtKSi.setDisable(selected);
            btnStart.setDisable(!selected);
            
            chart.setDisable(!selected);
            lwData.setDisable(!selected);
            
         }});

      btnStart.setOnAction(this::btnStartPressed);

      MenuItem item = new MenuItem("Odstranit");
      item.setOnAction(this::removeSelectedDataPoint);
      ContextMenu cm = new ContextMenu();
      cm.getItems()
        .add(item);

      ListViewUtils.setCellFactory(lwData, DataPoint::toString, cm);

      chart.setLegendVisible(false);
      chart.setAnimated(false);
      chart.getXAxis()
           .setLabel("1/d^2 [1/mm^2]");
      chart.getYAxis()
           .setLabel("dl/dt [mm/min]");
   }

   public CalibreEntry getEntry() {
      entry.setName(edtName.getText());

      entry.setKki(Double.parseDouble(edtKKi.getText()));
      entry.setKsi(Double.parseDouble(edtKSi.getText()));

      Map<Double, Double> dataPoints = new TreeMap<>();
      for (DataPoint dp : lwData.getItems()) {
         dataPoints.put(dp.getDiameterMm(), dp.getSpeedMmMin());
      }
      entry.setDataPoints(dataPoints);

      return entry;
   }

   public void setEntry(CalibreEntry entry) {
      this.entry = entry;
      edtName.setText(entry.getName());

      edtKKi.setText(Double.toString(entry.getKki()));
      edtKSi.setText(Double.toString(entry.getKsi()));

      Map<Double, Double> dataPoints = new TreeMap<>(entry.getDataPoints());
      lwData.getItems()
            .clear();
      for (Double key : dataPoints.keySet()) {
         addData(key, dataPoints.get(key));
      }
   }

   public void setInstrument(VlakVisInstrument instrument) {
      this.instrument = instrument;
   }

   public StringProperty getSampleNameProperty() {
      return edtName.textProperty();
   }

   public void addData(double diameterMm, double speedMmMin) {
      DataPoint dp = new DataPoint();
      dp.setDiameterMm(diameterMm);
      dp.setSpeedMmMin(speedMmMin);
      lwData.getItems()
            .add(dp);

      repaintResults();
   }

   private void repaintResults() {
      if (lwData.getItems()
                .size() <= 1) {
         edtKKi.setText("");
         edtKSi.setText("");
         chart.getData()
              .clear();
         return;
      }
      double s1 = 0, s2 = 0, s3 = 0, s4 = 0;
      int n = lwData.getItems()
                    .size();
      for (DataPoint dp : lwData.getItems()) {
         double x = dp.getDiameterMmPow();
         double y = dp.getSpeedMmMin();
         s1 += x * y;
         s2 += x;
         s3 += y;
         s4 += x * x;
      }
      // y = ax + b
      double a = (s1 - s2 * s3 / n) / (s4 - s2 * s2 / n);
      double b = (s3 - a * s2) / n;
      // y = kki * (x - ksi)
      double kki = a;
      double ksi = -b / a;

      edtKKi.setText(Double.toString(kki));
      edtKSi.setText(Double.toString(ksi));

      double xMin = 1e10, xMax = -1e10;
      Series<Double, Double> pointSerie = new Series<>();
      for (DataPoint dp : lwData.getItems()) {
         if (dp.getDiameterMmPow() < xMin) {
            xMin = dp.getDiameterMmPow();
         }
         if (dp.getDiameterMmPow() > xMax) {
            xMax = dp.getDiameterMmPow();
         }
         pointSerie.getData()
                   .add(new Data<>(dp.getDiameterMmPow(), dp.getSpeedMmMin()));
      }

      Series<Double, Double> fittedSerie = new Series<>();
      for (double x = xMin; x <= xMax; x += (xMax - xMin) / 50) {
         double y = kki * (x - ksi);
         fittedSerie.getData()
                    .add(new Data<>(x, y));
      }

      chart.getData()
           .clear();

      chart.getData()
           .add(pointSerie);
      pointSerie.nodeProperty()
                .get()
                .setStyle("-fx-stroke-width: 0px;");

      chart.getData()
           .add(fittedSerie);
      fittedSerie.getData()
                 .stream()
                 .forEach(d -> d.getNode()
                                .setVisible(false));

   }

   private void btnStartPressed(ActionEvent event) {
      GraphicControllerDTO gcDTO =
            AnchorPaneHelper.loadFXPaneByClass(CalibreMeasController.class);
      CalibreMeasController controller =
            (CalibreMeasController) gcDTO.getController();
      controller.setCalibrePaneController(this);
      controller.setInstrument(instrument);
      AnchorPaneHelper.buildDialog(gcDTO.getGraphic(), 500, 500,
                                   "Staticke Mereni");
   }

   private void removeSelectedDataPoint(ActionEvent event) {
      lwData.getItems()
            .remove(lwData.getSelectionModel()
                          .getSelectedIndex());
      repaintResults();
   }

   @lombok.Data
   private class DataPoint {

      private double diameterMm, speedMmMin;

      public String toString() {
         return "1/d^2: " + getDiameterMmPow() + " [1/mm^2], dl/dt: "
                + speedMmMin + " [mm/min]";
      }

      public double getDiameterMmPow() {
         if (diameterMm == 0) {
            return 0;
         }
         return Math.pow(diameterMm, -2);
      }
   }

}
