package cz.jlochman.marklar.vlak_vis.graphics.settings;

import java.net.URL;
import java.util.ResourceBundle;

import cz.jlochman.marklar.core.helpers.DecimalHelper;
import cz.jlochman.marklar.core.instruments.Instrument;
import cz.jlochman.marklar.core.logicModules.settingsServices.ExpInstanceSettingsService;
import cz.jlochman.marklar.core.logicModules.settingsServices.ExpTypeSettingsService;
import cz.jlochman.marklar.fxcommon.settings.experiment.ExperimentEditorSettingsPaneController;
import cz.jlochman.marklar.vlak_vis.experiment.settings.IVlakVisSettings;
import cz.jlochman.marklar.vlak_vis.experiment.settings.VlakVisExpTypeSettingsService;
import cz.jlochman.marklar.vlak_vis.experiment.settings.VlakVisInstanceSettingsService;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrument;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import lombok.extern.log4j.Log4j;

@Log4j
public class VlakVisExpSettingsController
      implements ExperimentEditorSettingsPaneController {

   @FXML
   private AnchorPane     anchorPane;
   @FXML
   private ComboBox<Long> cbReadOutInterval, cbPersistInterval;
   @FXML
   private TextField      edtStartTemp, edtLength, edtDiameter, edtTurningSpeed;

   private VlakVisInstrument              instrument;
   private VlakVisInstanceSettingsService instanceSS;
   private VlakVisExpTypeSettingsService  defaultSS;

   private final ObservableList<Long> READ_OUT_MILIS =
         FXCollections.observableArrayList(500L, 1000L, 1500L, 2000L, 3000L,
                                           4000L, 5000L);
   private final ObservableList<Long> PERSIST_MILIS  =
         FXCollections.observableArrayList(2000L, 3000L, 4000L, 5000L, 6000L,
                                           10000L, 12000L, 15000L, 20000L,
                                           30000L, 60000L);

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      cbReadOutInterval.getItems()
                       .addAll(READ_OUT_MILIS);
      cbPersistInterval.getItems()
                       .addAll(PERSIST_MILIS);
   }

   @Override
   public void setInstrument(Instrument instrument) {
      this.instrument = (VlakVisInstrument) this.instrument;
   }

   @Override
   public void setSettingsService(ExpInstanceSettingsService expInstSS) {
      this.instanceSS = (VlakVisInstanceSettingsService) expInstSS;
   }

   @Override
   public void setSettingsService(ExpTypeSettingsService expTypeSS) {
      this.defaultSS = (VlakVisExpTypeSettingsService) expTypeSS;
   }

   @Override
   public void loadSettings() {
      try {
         loadSettings(instanceSS);
      } catch (Exception e) {
         loadSettings(defaultSS);
      }
   }

   private void loadSettings(IVlakVisSettings settings) {
      cbReadOutInterval.setValue(settings.getIntervalReadOut());
      cbPersistInterval.setValue(settings.getIntervalPersist());

      edtDiameter.setText(DecimalHelper.toString(settings.getDiameter(), 3));
      edtLength.setText(DecimalHelper.toString(settings.getLength(), 2));
      edtStartTemp.setText(DecimalHelper.toString(settings.getStartT(), 1));
      edtTurningSpeed.setText(DecimalHelper.toString(settings.getSpeedTurningPoint(),
                                                     2));
   }

   @Override
   public void saveSettings() {
      if (instanceSS != null) {
         saveSettings(instanceSS);
      } else if (defaultSS != null) {
         saveSettings(defaultSS);
      } else {
         log.error("null settings");
      }
   }

   private void saveSettings(IVlakVisSettings settings) {
      settings.setIntervalReadOut(cbReadOutInterval.getValue());
      settings.setIntervalPersist(cbPersistInterval.getValue());

      try {
         settings.setDiameter(Double.parseDouble(edtDiameter.getText()));
         settings.setLength(Double.parseDouble(edtLength.getText()));
         settings.setStartT(Double.parseDouble(edtStartTemp.getText()));
         settings.setSpeedTurningPoint(Double.parseDouble(edtTurningSpeed.getText()));
      } catch (NumberFormatException e) {
         log.error("error parsing text to double", e);
      }
   }

   @Override
   public void processEvent(EventHandler<KeyEvent> keyEvent) {
      // TODO Auto-generated method stub

   }

   @Override
   public double getPrefHeight() {
      return anchorPane.getPrefHeight();
   }

}
