package cz.jlochman.marklar.vlak_vis.graphics.settings;

import java.util.HashMap;
import java.util.Map;

import cz.jlochman.marklar.core.helpers.DecimalHelper;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrumentSettingsService;
import lombok.Data;
import lombok.extern.log4j.Log4j;

@Data
@Log4j
public class CalibreEntry {

   static final String DATA_SEP = ";";

   private String name;
   private double kki, ksi;

   private Map<Double, Double> dataPoints = new HashMap<>();

   public static CalibreEntry fromString(String s) {
      if (s == null || s.isEmpty()) {
         return null;
      }
      try {
         CalibreEntry calibre = new CalibreEntry();
         String[] split = s.split(DATA_SEP);
         calibre.setName(split[0]);
         calibre.setKki(Double.parseDouble(split[1]));
         calibre.setKsi(Double.parseDouble(split[2]));

         Map<Double, Double> dataPoints = new HashMap<>();
         for (int i = 3; i < split.length; i += 2) {
            dataPoints.put(Double.parseDouble(split[i]),
                           Double.parseDouble(split[i + 1]));
         }
         calibre.setDataPoints(dataPoints);
         return calibre;
      } catch (Exception e) {
         log.error("error parsing calibre string:" + s, e);
         return null;
      }
   }

   public String asString() {
      if (name != null) {
         name = name.replaceAll(DATA_SEP, "")
                    .replace(VlakVisInstrumentSettingsService.CALIBER_SEP, "");
      }
      StringBuilder sb = new StringBuilder();
      sb.append(name)
        .append(DATA_SEP)
        .append(DecimalHelper.toString(kki, 5))
        .append(DATA_SEP)
        .append(DecimalHelper.toString(ksi, 5))
        .append(DATA_SEP);
      for (Double key : dataPoints.keySet()) {
         sb.append(DecimalHelper.toString(key, 5))
           .append(DATA_SEP)
           .append(DecimalHelper.toString(dataPoints.get(key), 5))
           .append(DATA_SEP);
      }
      return sb.toString();
   }

}
