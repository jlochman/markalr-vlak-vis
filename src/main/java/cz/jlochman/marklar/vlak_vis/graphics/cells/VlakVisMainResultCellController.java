package cz.jlochman.marklar.vlak_vis.graphics.cells;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import cz.jlochman.marklar.core.ServiceLocator;
import cz.jlochman.marklar.core.calculator.Statistic;
import cz.jlochman.marklar.core.calculator.StatisticCalculator;
import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.experiments.common.ExperimentStatus;
import cz.jlochman.marklar.core.graphicsDefinitions.GenericCell;
import cz.jlochman.marklar.core.helpers.DecimalHelper;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;
import cz.jlochman.marklar.vlak_vis.experiment.calculator.VlakVisCalculator;
import cz.jlochman.marklar.vlak_vis.graphics.settings.CalibreEntry;
import cz.jlochman.marklar.vlak_vis.graphics.settings.VlakVisCalibration;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j;

@Log4j
public class VlakVisMainResultCellController extends GenericCell {

   @FXML
   private Label                lblDcht, lblHcht, lblTrt;
   @FXML
   private ComboBox<CalibreDTO> cbCalibres;
   @FXML
   private CheckBox             cbUseCalibre;
   @FXML
   private Button               btnSave;
   @FXML
   private TextField            edtKki, edtKsi;

   private List<ExperimentInstance> expInstances;

   private final static double EPS = 1e-10;

   public VlakVisMainResultCellController(List<ExperimentInstance> expInstances) {
      super(null);
      this.expInstances = expInstances;
      setFXMLFile("VlakVisMainResultCell.fxml");
   }

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      lblDcht.setText("");
      lblHcht.setText("");
      lblTrt.setText("");

      edtKki.disableProperty()
            .bind(cbUseCalibre.selectedProperty());
      edtKki.setOnKeyReleased(new EventHandler<KeyEvent>() {

         @Override
         public void handle(KeyEvent event) {
            event.consume();
         }
      });

      edtKsi.disableProperty()
            .bind(cbUseCalibre.selectedProperty());
      edtKsi.setOnKeyReleased(new EventHandler<KeyEvent>() {

         @Override
         public void handle(KeyEvent event) {
            event.consume();
         }
      });

      cbCalibres.disableProperty()
                .bind(cbUseCalibre.selectedProperty()
                                  .not());

      cbCalibres.valueProperty()
                .addListener(new ChangeListener<CalibreDTO>() {

                   @Override
                   public void
                         changed(ObservableValue<? extends CalibreDTO> observable,
                                 CalibreDTO oldValue, CalibreDTO newValue) {
                      if (newValue != null) {
                         edtKki.setText(Double.toString(newValue.getEntry()
                                                                .getKki()));
                         edtKsi.setText(Double.toString(newValue.getEntry()
                                                                .getKsi()));
                      }

                   }
                });

      btnSave.setOnAction(this::savePressed);
   }

   @Override
   public void fillGraphics() {
      setBackgroundColor();
      if (expInstances == null || expInstances.isEmpty()) {
         return;
      }

      setBackgroundColor(Color.LIGHTBLUE);

      ExperimentInstance expInstance = expInstances.get(0);
      VlakVisDataService dataService =
            (VlakVisDataService) expInstance.getDataService();
      long instrumentId = expInstance.getInstrument()
                                     .getId();

      List<CalibreDTO> calibreDTOs = new ArrayList<>();
      for (CalibreEntry calibre : VlakVisCalibration.getCalibres(instrumentId)) {
         calibreDTOs.add(new CalibreDTO(calibre));
      }
      cbCalibres.getItems()
                .clear();
      cbCalibres.getItems()
                .addAll(calibreDTOs);

      double kki = dataService.loadKki();
      double ksi = dataService.loadKsi();

      CalibreDTO selectedCalibre = null;
      for (CalibreDTO calibre : cbCalibres.getItems()) {
         if (Math.abs(calibre.getEntry()
                             .getKki()
                      - kki) < EPS
             && Math.abs(calibre.getEntry()
                                .getKsi()
                         - ksi) < EPS) {
            selectedCalibre = calibre;
            break;
         }
      }
      if (selectedCalibre != null) {
         cbCalibres.getSelectionModel()
                   .select(selectedCalibre);
         cbUseCalibre.setSelected(true);
      } else {
         edtKki.setText(Double.toString(kki));
         edtKsi.setText(Double.toString(ksi));
         cbUseCalibre.setSelected(false);
      }

      List<Double> dchts = new ArrayList<>();
      List<Double> hchts = new ArrayList<>();
      List<Double> trts = new ArrayList<>();
      for (ExperimentInstance instance : expInstances) {
         instance = ServiceLocator.getInstance()
                                  .getExperimentService()
                                  .getExperimentByID(instance.getId());
         if (instance == null) {
            continue;
         }

         Object status = instance.getStatus();
         if (status == ExperimentStatus.ENDED
             || status == ExperimentStatus.ABORTED) {
            VlakVisDataService ds =
                  (VlakVisDataService) instance.getDataService();
            dchts.add(ds.loadDcht());
            hchts.add(ds.loadHcht());
            trts.add(ds.loadTrt());
         }
      }
      fillDchtHchtTrt(dchts, hchts, trts);
      savePressed(null);
   }

   private void fillDchtHchtTrt(List<Double> dchts, List<Double> hchts,
                                List<Double> trts) {
      dchts = dchts.stream()
                   .filter(x -> x > 0)
                   .collect(Collectors.toList());
      if (!dchts.isEmpty()) {
         Statistic stats = StatisticCalculator.getStatistic(dchts);
         if (dchts.size() > 1) {
            lblDcht.setText(stats.asString(1, 1) + " ˚C");
         } else {
            lblDcht.setText(DecimalHelper.toString(stats.getMean(), 1) + " ˚C");
         }
      } else {
         lblDcht.setText("");
      }

      hchts = hchts.stream()
                   .filter(x -> x > 0)
                   .collect(Collectors.toList());
      if (!hchts.isEmpty()) {
         Statistic stats = StatisticCalculator.getStatistic(hchts);
         if (hchts.size() > 1) {
            lblHcht.setText(stats.asString(1, 1) + " ˚C");
         } else {
            lblHcht.setText(DecimalHelper.toString(stats.getMean(), 1) + " ˚C");
         }
      } else {
         lblHcht.setText("");
      }

      trts = trts.stream()
                 .filter(x -> x > 0)
                 .collect(Collectors.toList());
      if (!trts.isEmpty()) {
         Statistic stats = StatisticCalculator.getStatistic(trts);
         if (trts.size() > 1) {
            lblTrt.setText(stats.asString(1, 1) + " ˚C");
         } else {
            lblTrt.setText(DecimalHelper.toString(stats.getMean(), 1) + " ˚C");
         }
      } else {
         lblTrt.setText("");
      }
   }

   private void savePressed(ActionEvent event) {
      double kki, ksi;
      try {
         kki = Double.parseDouble(edtKki.getText());
         ksi = Double.parseDouble(edtKsi.getText());
      } catch (Exception e) {
         log.error("kki or ksi is not a number: " + edtKki.getText() + " "
                   + edtKsi.getText());
         return;
      }

      List<Double> dchts = new ArrayList<>();
      List<Double> hchts = new ArrayList<>();
      List<Double> trts = new ArrayList<>();
      for (ExperimentInstance instance : expInstances) {
         instance = ServiceLocator.getInstance()
                                  .getExperimentService()
                                  .getExperimentByID(instance.getId());
         if (instance == null) {
            continue;
         }
         Object status = instance.getStatus();
         if (status == ExperimentStatus.ENDED
             || status == ExperimentStatus.ABORTED) {
            VlakVisDataService dataService =
                  (VlakVisDataService) instance.getDataService();
            dataService.saveKki(kki);
            dataService.saveKsi(ksi);

            VlakVisCalculator calc = new VlakVisCalculator(instance);
            calc.calculate();
            calc.save();

            ServiceLocator.getInstance()
                          .getLogicModuleManager()
                          .raiseEvent(new ResultTemperaturesChangedEvent(calc.getDcht(),
                                                                         calc.getHcht(),
                                                                         calc.getTrt()),
                                      instance);
            dchts.add(calc.getDcht());
            hchts.add(calc.getHcht());
            trts.add(calc.getTrt());
         }
      }

      fillDchtHchtTrt(dchts, hchts, trts);
   }

   @Data
   @AllArgsConstructor
   private class CalibreDTO {

      private CalibreEntry entry;

      @Override
      public String toString() {
         return entry == null ? "NULL" : entry.getName();
      }
   }

}
