package cz.jlochman.marklar.vlak_vis.graphics.right;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import cz.jlochman.marklar.core.ServiceLocator;
import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.experiments.common.ExperimentStatus;
import cz.jlochman.marklar.core.experiments.common.ExperimentStatusHelper;
import cz.jlochman.marklar.core.graphicsDefinitions.AnchorPaneHelper;
import cz.jlochman.marklar.core.helpers.DecimalHelper;
import cz.jlochman.marklar.core.language.I18n;
import cz.jlochman.marklar.core.logicModules.manager.LogicModuleManager;
import cz.jlochman.marklar.core.logicModules.observer.ExperimentLogicListener;
import cz.jlochman.marklar.core.logicModules.observer.LogicModuleEvent;
import cz.jlochman.marklar.fxcommon.rightPane.ControllPaneController;
import cz.jlochman.marklar.vlak_vis.experiment.LengthSpeedReadOutEvent;
import cz.jlochman.marklar.vlak_vis.experiment.NewRunningSubStatusEvent;
import cz.jlochman.marklar.vlak_vis.experiment.RunningSubstatus;
import cz.jlochman.marklar.vlak_vis.experiment.TempReadOutEvent;
import cz.jlochman.marklar.vlak_vis.experiment.settings.VlakVisInstanceSettingsService;
import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.GaugeBuilder;
import eu.hansolo.medusa.Section;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import lombok.Getter;

public class VlakVisControlPaneController extends ControllPaneController {

   @FXML
   private Label      lblStatus, lblTemp, lblLength, lblSpeed, lblInfo;
   @FXML
   private AnchorPane mainPane, statusButtonsPane, gaugePane;
   @FXML
   private Button     btnStart;

   private ButtonHandler             btnHandler;
   private TempGauge                 tempGauge;
   @FXML
   private LineChart<Double, Double> speedChart;
   @FXML
   private NumberAxis                xAxis;
   private ChartData                 chartData = new ChartData();

   private static ExperimentLogicListener instanceListener;

   private VlakVisInstanceSettingsService instanceSS;

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      xAxis.setAutoRanging(false);
      xAxis.setLowerBound(-ChartData.KEEP_HISTORY_MILLIS / 1000);
      xAxis.setUpperBound(0);
      xAxis.setTickUnit(60);

      speedChart.setLegendVisible(false);
      speedChart.setAnimated(false);
      speedChart.getYAxis()
                .setLabel("rychlost [mm/min]");

      btnStart.setOnAction(this::startPressed);
   }

   @Override
   public void setExpInstance(ExperimentInstance expInstance) {
      super.setExpInstance(expInstance);
      instanceSS =
            (VlakVisInstanceSettingsService) expInstance.getSettignsService();
      btnStart.setVisible(instanceSS.getRunningSubstatus() == RunningSubstatus.INITIAL_TEMP
                          && expInstance.getStatus() == ExperimentStatus.RUNNING);
      setLblInfo(instanceSS.getRunningSubstatus());

      tempGauge = new TempGauge(instanceSS.getStartT());

      btnHandler = new ButtonHandler(expInstance);
      handleExperimentStatus(expInstance.getStatus());

      if (instanceListener != null) {
         ServiceLocator.getInstance()
                       .getLogicModuleManager()
                       .unregisterListener(expInstance, instanceListener);
      }
      instanceListener = new ExperimentLogicListener() {

         @Override
         public void processEvent(LogicModuleEvent event) {
            if (event instanceof TempReadOutEvent) {
               TempReadOutEvent pevent = (TempReadOutEvent) event;
               lblTemp.setText(DecimalHelper.toString(pevent.getTemperature(),
                                                      1)
                               + " ˚C");
               tempGauge.setTemperature(pevent.getTemperature());
            } else if (event instanceof LengthSpeedReadOutEvent) {
               LengthSpeedReadOutEvent pevent = (LengthSpeedReadOutEvent) event;

               lblLength.setText(DecimalHelper.toString(pevent.getLengthMm(), 3)
                                 + " mm");
               lblSpeed.setText(DecimalHelper.toString(pevent.getSpeedMmMin(),
                                                       3)
                                + " mm/min");

               chartData.addPoint(System.currentTimeMillis(),
                                  pevent.getSpeedMmMin());
               speedChart.getData()
                         .clear();
               speedChart.getData()
                         .add(chartData.getSerie());
            } else if (event instanceof NewRunningSubStatusEvent) {
               NewRunningSubStatusEvent pevent =
                     (NewRunningSubStatusEvent) event;
               RunningSubstatus substatus = pevent.getRunningSubstatus();
               btnStart.setVisible(substatus == RunningSubstatus.INITIAL_TEMP);
               setLblInfo(instanceSS.getRunningSubstatus());
            }
         }
      };
      ServiceLocator.getInstance()
                    .getLogicModuleManager()
                    .registerListener(expInstance, instanceListener);
   }

   private void setLblInfo(RunningSubstatus substatus) {
      if (expInstance.getStatus() != ExperimentStatus.RUNNING) {
         lblInfo.setText("");
         return;
      }
      switch (substatus) {
      case INITIAL_TEMP:
         lblInfo.setText("Vyckejte na vyhrati na " + instanceSS.getStartT()
                         + " ˚C, vlozte vzorek, stisknete START");
         break;
      case INCREASING_TEMP:
         lblInfo.setText("Pri rychlosti > "
                         + DecimalHelper.toString(instanceSS.getSpeedTurningPoint(),
                                                  2)
                         + " mm/min prejde mereni automaticky do dalsi faze");
         break;
      case DECREASING_TEMP:
         lblInfo.setText("Pri rychlosti < 0.1 mm/min se mereni automaticky ukonci");
         break;
      case FINISHED:
         lblInfo.setText("");
         break;
      }
   }

   private void startPressed(ActionEvent event) {
      ServiceLocator.getInstance()
                    .getLogicModuleManager()
                    .raiseEvent(new NewRunningSubStatusEvent(RunningSubstatus.INCREASING_TEMP),
                                expInstance);
   }

   private void handleExperimentStatus(ExperimentStatus status) {
      if (btnHandler != null) {
         AnchorPaneHelper.insertNodeToContent(btnHandler.getGraphics(status),
                                              statusButtonsPane);
      }
      lblStatus.setText(ExperimentStatusHelper.toString(status));
      lblStatus.setTextFill(ExperimentStatusHelper.toColor(status));

      if (tempGauge != null) {
         if (status == ExperimentStatus.RUNNING) {
            AnchorPaneHelper.insertNodeToContent(tempGauge.getGauge(),
                                                 gaugePane);
         } else {
            gaugePane.getChildren()
                     .clear();
         }
      }

      speedChart.setVisible(status == ExperimentStatus.RUNNING);
      if (chartData != null && status == ExperimentStatus.RUNNING) {
         speedChart.getData()
                   .clear();
         speedChart.getData()
                   .add(chartData.getSerie());
      }
   }

   @Override
   public void clearGraphics() {
      mainPane.getChildren()
              .clear();
   }

   @Override
   public void repaint() {
      lblTemp.setText("");
      lblLength.setText("");
      lblSpeed.setText("");
   }

   @Override
   public void newData(Object dataRow) {
   }

   private class ChartData {

      private ConcurrentMap<Long, Double> dataMap = new ConcurrentHashMap<>();
      private long                        lastTime;

      private final static long KEEP_HISTORY_MILLIS = 120_000L;

      public void addPoint(long time, double value) {
         lastTime = time;
         synchronized (dataMap) {
            dataMap.put(time, value);
            for (long pastTime : dataMap.keySet()) {
               if (time - pastTime > KEEP_HISTORY_MILLIS) {
                  dataMap.remove(pastTime);
               }
            }
         }
      }

      public Series<Double, Double> getSerie() {
         Series<Double, Double> serie = new Series<>();
         synchronized (dataMap) {
            for (long time : dataMap.keySet()) {
               serie.getData()
                    .add(new Data<>(((double) (time - lastTime)) / 1000,
                                    dataMap.get(time)));
            }
         }
         return serie;
      }

   }

   private class TempGauge {

      @Getter
      private Gauge gauge;

      private final static double BELOW_START_T = 150;
      private final static double ABOVE_START_T = 150;
      private final static double GOOD_RANGE    = 5;

      public TempGauge(double startT) {
         gauge = GaugeBuilder.create()
                             .sectionsVisible(true)
                             .checkSectionsForValue(true)
                             .highlightSections(true)
                             .subTitle("˚C")
                             .minValue(startT - BELOW_START_T)
                             .maxValue(startT + ABOVE_START_T)
                             .value(0)
                             .sections(new Section(startT - BELOW_START_T,
                                                   startT - GOOD_RANGE,
                                                   Color.TRANSPARENT),
                                       new Section(startT - GOOD_RANGE,
                                                   startT + GOOD_RANGE,
                                                   Color.rgb(0, 255, 0, 0.3)),
                                       new Section(startT + GOOD_RANGE,
                                                   startT + ABOVE_START_T,
                                                   Color.TRANSPARENT))
                             .build();
      }

      public void setTemperature(double temp) {
         gauge.setValue(temp);
      }

   }

   private class ButtonHandler {

      private ExperimentInstance instance;
      private LogicModuleManager logicModuleManager;

      private final static double SPACING = 10.;

      public ButtonHandler(ExperimentInstance expInstance) {
         this.instance = expInstance;
         logicModuleManager = ServiceLocator.getInstance()
                                            .getLogicModuleManager();
      }

      public Node getGraphics(ExperimentStatus status) {
         List<Button> buttons = new ArrayList<>();
         switch (status) {
         case NEW:
            Button btnLock = new Button(I18n.getGraphicsBundle()
                                            .getString("lockMeasurement"));
            btnLock.setOnAction(e -> logicModuleManager.lockInstance(instance));
            buttons.add(btnLock);

            break;
         case NEW_LOCKED:
            Button btnUnlock = new Button(I18n.getGraphicsBundle()
                                              .getString("unlockMeasurement"));
            btnUnlock.setOnAction(e -> logicModuleManager.unLockInstance(instance));
            buttons.add(btnUnlock);

            Button btnStart = new Button(I18n.getGraphicsBundle()
                                             .getString("startMeasurement"));
            btnStart.setOnAction(e -> logicModuleManager.runInstance(instance));
            buttons.add(btnStart);

            break;
         case RUNNING:
            Button btnEnd = new Button(I18n.getGraphicsBundle()
                                           .getString("endMeasurement"));
            btnEnd.setOnAction(e -> logicModuleManager.abortInstance(instance));
            buttons.add(btnEnd);

            break;
         case SUSPENDED:
            Button btnAbort = new Button(I18n.getGraphicsBundle()
                                             .getString("markAsSuccessfulMeasurement"));
            btnAbort.setOnAction(e -> logicModuleManager.abortInstance(instance));
            buttons.add(btnAbort);

            break;
         case ABORTED:
         case ENDED:
            Button btnScrap = new Button(I18n.getGraphicsBundle()
                                             .getString("scrapMeasurement"));
            btnScrap.setOnAction(e -> logicModuleManager.suspendInstance(instance));
            buttons.add(btnScrap);

            break;
         case CRASHED:
            Button btnResurect = new Button(I18n.getGraphicsBundle()
                                                .getString("resurectMeasurement"));
            btnResurect.setOnAction(e -> logicModuleManager.restoreInstance(instance));
            buttons.add(btnResurect);

            break;
         case PAUSED:
            Button btnContinue = new Button(I18n.getGraphicsBundle()
                                                .getString("continueMeasurement"));
            btnContinue.setOnAction(e -> {
               logicModuleManager.unpauseInstance(instance);
            });
            buttons.add(btnContinue);

            Button btnScrap2 = new Button(I18n.getGraphicsBundle()
                                              .getString("scrapMeasurement"));
            btnScrap2.setOnAction((ActionEvent event) -> {
               logicModuleManager.suspendInstance(instance);
            });
            buttons.add(btnScrap2);
            break;
         }

         HBox hBox = new HBox(SPACING);
         hBox.setAlignment(Pos.CENTER_LEFT);
         buttons.stream()
                .forEach(hBox.getChildren()::add);
         return hBox;
      }

   }

}
