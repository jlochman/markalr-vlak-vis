package cz.jlochman.marklar.vlak_vis.graphics;

import java.util.List;

import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.graphicsDefinitions.AnchorPaneHelper;
import cz.jlochman.marklar.core.graphicsDefinitions.GenericCell;
import cz.jlochman.marklar.core.graphicsDefinitions.GraphicControllerDTO;
import cz.jlochman.marklar.core.instruments.Instrument;
import cz.jlochman.marklar.core.logicModules.definitions.ExperimentTypeGraphicFactory;
import cz.jlochman.marklar.vlak_vis.graphics.cells.VlakVisCrashedCellController;
import cz.jlochman.marklar.vlak_vis.graphics.cells.VlakVisEndedCellController;
import cz.jlochman.marklar.vlak_vis.graphics.cells.VlakVisMainResultCellController;
import cz.jlochman.marklar.vlak_vis.graphics.cells.VlakVisPausedCellController;
import cz.jlochman.marklar.vlak_vis.graphics.cells.VlakVisPreparedCellController;
import cz.jlochman.marklar.vlak_vis.graphics.cells.VlakVisRunningCellController;
import cz.jlochman.marklar.vlak_vis.graphics.right.VlakVisControlPaneController;
import cz.jlochman.marklar.vlak_vis.graphics.right.VlakVisInfoTabController;
import cz.jlochman.marklar.vlak_vis.graphics.right.VlakVisTabsController;
import cz.jlochman.marklar.vlak_vis.graphics.settings.VlakVisCallibrationPaneController;
import cz.jlochman.marklar.vlak_vis.graphics.settings.VlakVisCorrectionerPaneController;
import cz.jlochman.marklar.vlak_vis.graphics.settings.VlakVisExpSettingsController;
import cz.jlochman.marklar.vlak_vis.graphics.settings.VlakVisProtocolPaneController;

public class VlakVisGraphicFactory implements ExperimentTypeGraphicFactory {

   @Override
   public GraphicControllerDTO getSettingsGraphic() {
      return AnchorPaneHelper.loadFXPaneByClass(VlakVisExpSettingsController.class);
   }

   @Override
   public GraphicControllerDTO
         getMeasurementControllPanel(ExperimentInstance expInstance) {
      GraphicControllerDTO gcDTO =
            AnchorPaneHelper.loadFXPaneByClass(VlakVisControlPaneController.class);
      ((VlakVisControlPaneController) gcDTO.getController()).setExpInstance(expInstance);
      return gcDTO;
   }

   @Override
   public GraphicControllerDTO getMeasurementFixedControllPanel() {
      // TODO: mozna bude dobry dat neco stranou
      return null;
   }

   @Override
   public GraphicControllerDTO
         getExperimentTabs(ExperimentInstance expInstance) {
      GraphicControllerDTO gcDTO =
            AnchorPaneHelper.loadFXPaneByClass(VlakVisTabsController.class);
      ((VlakVisTabsController) gcDTO.getController()).setExpInstance(expInstance);
      return gcDTO;
   }

   @Override
   public GraphicControllerDTO getCallibrationGraphic(Instrument instrument) {
      GraphicControllerDTO gcDTO =
            AnchorPaneHelper.loadFXPaneByClass(VlakVisCallibrationPaneController.class);
      ((VlakVisCallibrationPaneController) gcDTO.getController()).setInstrument(instrument);
      return gcDTO;
   }

   @Override
   public GraphicControllerDTO getProtocolGraphic() {
      return AnchorPaneHelper.loadFXPaneByClass(VlakVisProtocolPaneController.class);
   }

   @Override
   public GenericCell
         getMainTableCellForInstance(ExperimentInstance expInstance) {
      switch (expInstance.getStatus()) {
      case NEW:
      case NEW_LOCKED:
         return new VlakVisPreparedCellController(expInstance);
      case RUNNING:
         return new VlakVisRunningCellController(expInstance);
      case SUSPENDED:
      case CRASHED:
         return new VlakVisCrashedCellController(expInstance);
      case ENDED:
      case ABORTED:
         return new VlakVisEndedCellController(expInstance);
      case PAUSED:
         return new VlakVisPausedCellController(expInstance);
      }
      return null;
   }

   @Override
   public GenericCell
         getMainResultCellForInstances(List<ExperimentInstance> expInstances) {
      return new VlakVisMainResultCellController(expInstances);
   }

   @Override
   public GraphicControllerDTO
         getCorrectionersGraphics(Instrument actualInstrument) {
      GraphicControllerDTO gcDTO =
            AnchorPaneHelper.loadFXPaneByClass(VlakVisCorrectionerPaneController.class);
      ((VlakVisCorrectionerPaneController) gcDTO.getController()).setInstrument(actualInstrument);
      return gcDTO;
   }

   @Override
   public GraphicControllerDTO
         getExperimentInfoTab(ExperimentInstance expInstance) {
      GraphicControllerDTO gcDTO =
            AnchorPaneHelper.loadFXPaneByClass(VlakVisInfoTabController.class);
      ((VlakVisInfoTabController) gcDTO.getController()).setExpInstance(expInstance);
      return gcDTO;
   }

}
