package cz.jlochman.marklar.vlak_vis.graphics.cells;

import java.net.URL;
import java.util.ResourceBundle;

import cz.jlochman.marklar.core.ServiceLocator;
import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.graphicsDefinitions.GenericCell;
import cz.jlochman.marklar.core.helpers.DecimalHelper;
import cz.jlochman.marklar.core.logicModules.observer.ExperimentLogicListener;
import cz.jlochman.marklar.core.logicModules.observer.LogicModuleEvent;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;
import cz.jlochman.marklar.vlak_vis.experiment.LengthSpeedReadOutEvent;
import cz.jlochman.marklar.vlak_vis.experiment.NewDataEvent;
import cz.jlochman.marklar.vlak_vis.experiment.TempReadOutEvent;
import cz.jlochman.marklar.vlak_vis.graphics.VlakVisChart;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class VlakVisRunningCellController extends GenericCell {

   @FXML
   private Label        lblTemp, lblLength, lblSpeed;
   @FXML
   private AnchorPane   chartPane;
   private VlakVisChart chart;

   private VlakVisDataService dataService;

   private ExperimentLogicListener expInstanceListener;

   public VlakVisRunningCellController(ExperimentInstance expInst) {
      super(expInst);
      setFXMLFile("VlakVisRunningCell.fxml");

      dataService = (VlakVisDataService) expInst.getDataService();

      expInstanceListener = new ExperimentLogicListener() {

         @Override
         public void processEvent(LogicModuleEvent event) {
            if (event instanceof TempReadOutEvent) {
               TempReadOutEvent pevent = (TempReadOutEvent) event;
               lblTemp.setText(DecimalHelper.toString(pevent.getTemperature(),
                                                      1)
                               + " ˚C");
            } else if (event instanceof LengthSpeedReadOutEvent) {
               LengthSpeedReadOutEvent pevent = (LengthSpeedReadOutEvent) event;
               lblLength.setText(DecimalHelper.toString(pevent.getLengthMm(), 3)
                                 + " mm");
               lblSpeed.setText(DecimalHelper.toString(pevent.getSpeedMmMin(),
                                                       3)
                                + " mm/min");
            } else if (event instanceof NewDataEvent) {
               NewDataEvent pevent = (NewDataEvent) event;
               chart.addData(pevent.getDataRow());
            }

         }
      };
      ServiceLocator.getInstance()
                    .getLogicModuleManager()
                    .registerListener(expInst, expInstanceListener);
   }

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      // TODO Auto-generated method stub

   }

   @Override
   protected void finalize() throws Throwable {
      super.finalize();
      if (expInstanceListener != null) {
         ServiceLocator.getInstance()
                       .getLogicModuleManager()
                       .unregisterListener(expInst, expInstanceListener);
      }
   }

   @Override
   public void fillGraphics() {
      setBackgroundColor();

      lblTemp.setText("");
      lblLength.setText("");
      lblSpeed.setText("");

      chart = new VlakVisChart(chartPane, true);
      chart.repaint(dataService);
   }

}
