package cz.jlochman.marklar.vlak_vis.graphics.settings;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import cz.jlochman.marklar.core.instruments.Instrument;
import cz.jlochman.marklar.fxcommon.settings.callibration.InstrumentCallibrationPaneController;
import cz.jlochman.marklar.fxcommon.utils.DialogUtils;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrument;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrumentSettingsService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TabPane;
import lombok.extern.log4j.Log4j;

@Log4j
public class VlakVisCallibrationPaneController
      extends InstrumentCallibrationPaneController {

   @FXML
   private Button  btnInfo, btnAddCaliber, btnRemoveCaliber;
   @FXML
   private TabPane tabPane;

   private List<CalibreTab> tabs = new ArrayList<>();

   private VlakVisInstrument                instrument;
   private VlakVisInstrumentSettingsService instrumentSS;

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      btnInfo.setOnAction(this::btnInfoPressed);
      btnAddCaliber.setOnAction(this::addCaliberPressed);
      btnRemoveCaliber.setOnAction(this::removeCaliberPressed);
   }

   @Override
   public void setInstrument(Instrument instrument) {
      super.setInstrument(instrument);
      this.instrument = (VlakVisInstrument) instrument;
      instrumentSS =
            (VlakVisInstrumentSettingsService) instrument.getSettingsService();

      List<CalibreEntry> calibres =
            VlakVisCalibration.getCalibres(instrument.getId());
      calibresToGraphics(calibres);
   }

   @Override
   public void save() {
      List<CalibreEntry> calibres = graphicsToCalibres();
      instrumentSS.saveCalibres(calibres);
   }

   private void calibresToGraphics(List<CalibreEntry> calibres) {
      tabPane.getTabs()
             .clear();
      tabs.clear();
      for (CalibreEntry entry : calibres) {
         CalibreTab cTab = new CalibreTab(entry, instrument);
         tabs.add(cTab);
         tabPane.getTabs()
                .add(cTab.getTab());
      }
   }

   private List<CalibreEntry> graphicsToCalibres() {
      List<CalibreEntry> calibres = new ArrayList<>();
      for (CalibreTab tab : tabs) {
         calibres.add(tab.getCalibre());
      }
      return calibres;
   }

   private void btnInfoPressed(ActionEvent event) {
      Alert alert =
            DialogUtils.buildSimpleDialog("Kalibrace", "Informace ke Kalibraci",
                                          "dlooohy text" + System.lineSeparator() + "test",
                                          AlertType.INFORMATION);
      alert.showAndWait();
   }

   private void addCaliberPressed(ActionEvent event) {
      CalibreTab cTab = new CalibreTab(new CalibreEntry(), instrument);
      tabs.add(cTab);
      tabPane.getTabs()
             .add(cTab.getTab());
      tabPane.getSelectionModel()
             .selectLast();
   }

   private void removeCaliberPressed(ActionEvent event) {
      Alert alert =
            DialogUtils.buildSimpleDialog("Odstranit Kalibr?",
                                          "Prejete si odstranit kalibr?",
                                          "Kalibr bude nenavratne odstranen",
                                          AlertType.CONFIRMATION);
      Optional<ButtonType> result = alert.showAndWait();
      if (result.isPresent() && result.get() == ButtonType.OK) {
         int idx = tabPane.getSelectionModel()
                          .getSelectedIndex();
         log.info("removing kalibr: " + tabs.get(idx)
                                            .getCalibre()
                                            .asString());
         tabs.remove(idx);
         tabPane.getTabs()
                .remove(idx);
      }
   }

}
