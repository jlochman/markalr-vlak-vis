package cz.jlochman.marklar.vlak_vis.graphics.settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class PowerRestriction {

   @Getter
   private List<PowerRestrictionEntry> restrictions = new ArrayList<>();

   private final static String ENTRY_SEP = "@";
   private final static String VALUE_SEP = ";";

   public PowerRestrictionEntry getRestriction(double temp) {
      if (restrictions == null || restrictions.isEmpty()) {
         return new PowerRestrictionEntry(0, 1000, 0, 100);
      }
      return restrictions.stream()
                         .sorted((e1,
                                  e2) -> Double.compare(e1.getTempHeuristics(temp),
                                                        e2.getTempHeuristics(temp)))
                         .findFirst()
                         .get();
   }

   public String asString() {
      return restrictions.stream()
                         .map(this::entryAsString)
                         .collect(Collectors.joining(ENTRY_SEP));
   }

   private String entryAsString(PowerRestrictionEntry entry) {
      StringBuilder sb = new StringBuilder();
      sb.append(entry.getTempFromIncl());
      sb.append(VALUE_SEP);
      sb.append(entry.getTempToExcl());
      sb.append(VALUE_SEP);
      sb.append(entry.getLowLimit());
      sb.append(VALUE_SEP);
      sb.append(entry.getHighLimit());
      return sb.toString();
   }

   public static PowerRestriction fromString(String s) {
      List<PowerRestrictionEntry> restList = Arrays.stream(s.split(ENTRY_SEP))
                                                   .map(PowerRestriction::entryFromString)
                                                   .collect(Collectors.toList());
      return new PowerRestriction(restList);
   }

   private static PowerRestrictionEntry entryFromString(String s) {
      String[] split = s.split(VALUE_SEP);
      return new PowerRestrictionEntry(Integer.parseInt(split[0]),
                                       Integer.parseInt(split[1]),
                                       Integer.parseInt(split[2]),
                                       Integer.parseInt(split[3]));
   }

   public static void main(String[] args) {
      List<PowerRestrictionEntry> restrictions = new ArrayList<>();
      restrictions.add(new PowerRestrictionEntry(100, 150, 10, 20));
      restrictions.add(new PowerRestrictionEntry(150, 200, 20, 30));
      restrictions.add(new PowerRestrictionEntry(200, 250, 30, 40));
      restrictions.add(new PowerRestrictionEntry(250, 300, 40, 50));
      PowerRestriction restriction = new PowerRestriction(restrictions);

      System.out.println(restriction.getRestriction(50));
      System.out.println(restriction.getRestriction(100));
      System.out.println(restriction.getRestriction(200));
      System.out.println(restriction.getRestriction(290));
      System.out.println(restriction.getRestriction(300));
   }

}
