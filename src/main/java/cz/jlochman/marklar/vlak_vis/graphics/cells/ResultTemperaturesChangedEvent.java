package cz.jlochman.marklar.vlak_vis.graphics.cells;

import cz.jlochman.marklar.core.logicModules.observer.LogicModuleEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class ResultTemperaturesChangedEvent extends LogicModuleEvent {

   @Getter
   private final double dcht, hcht, trt;

}
