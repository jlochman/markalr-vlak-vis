package cz.jlochman.marklar.vlak_vis.graphics.settings;

import cz.jlochman.marklar.core.graphicsDefinitions.AnchorPaneHelper;
import cz.jlochman.marklar.core.graphicsDefinitions.GraphicControllerDTO;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrument;
import javafx.scene.control.Tab;

class CalibreTab {

   private CalibrePaneController paneController;

   private Tab tab;

   public CalibreTab(CalibreEntry entry, VlakVisInstrument
                      instrument) {
      GraphicControllerDTO gcDTO =
            AnchorPaneHelper.loadFXPaneByClass(CalibrePaneController.class);
      paneController = (CalibrePaneController) gcDTO.getController();
      paneController.setEntry(entry);
      paneController.setInstrument(instrument);

      tab = new Tab(entry.getName(), gcDTO.getGraphic());
      tab.textProperty()
         .bind(paneController.getSampleNameProperty());
   }

   public CalibreEntry getCalibre() {
      return paneController.getEntry();
   }

   public Tab getTab() {
      return tab;
   }

}
