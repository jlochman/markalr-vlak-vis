package cz.jlochman.marklar.vlak_vis.graphics.settings;

import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import cz.jlochman.marklar.core.ServiceLocator;
import cz.jlochman.marklar.core.graphicsDefinitions.AnchorPaneHelper;
import cz.jlochman.marklar.core.graphicsDefinitions.GraphicControllerDTO;
import cz.jlochman.marklar.core.instruments.Instrument;
import cz.jlochman.marklar.fxcommon.settings.correctioner.CorrectionTabController;
import cz.jlochman.marklar.fxcommon.settings.correctioner.InstrumentCorrectionPaneController;
import cz.jlochman.marklar.fxcommon.utils.ListViewUtils;
import cz.jlochman.marklar.vlak_vis.instrument.TempCorrectionerEvent;
import cz.jlochman.marklar.vlak_vis.instrument.VlakVisInstrumentSettingsService;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class VlakVisCorrectionerPaneController
      extends InstrumentCorrectionPaneController {

   @FXML
   private AnchorPane tempPane, powRestrictionPane;

   private VlakVisInstrumentSettingsService instrumentSS;

   private CorrectionTabController tempCorr;
   private PowerRestrictionPane    prPane;

   @Override
   public void initialize(URL location, ResourceBundle resources) {
   }

   @Override
   public void save() {
      instrumentSS.saveTempCorrectioner(tempCorr.getCorrectioner()
                                                .toString());
      ServiceLocator.getInstance()
                    .getLogicModuleManager()
                    .raiseEvent(new TempCorrectionerEvent(tempCorr.getCorrectioner()
                                                                  .toString()),
                                instrument);
      instrumentSS.savePowerRestriction(prPane.getRestriction());
   }

   @Override
   public void setInstrument(Instrument instrument) {
      super.setInstrument(instrument);
      instrumentSS =
            (VlakVisInstrumentSettingsService) instrument.getSettingsService();

      GraphicControllerDTO gcDTO;

      gcDTO = createCorrectionTabController();
      tempCorr = (CorrectionTabController) gcDTO.getController();
      AnchorPaneHelper.insertNodeToContent(gcDTO.getGraphic(), tempPane);
      tempCorr.setCorrectioner(instrumentSS.getTempCorrectioner());

      prPane = new PowerRestrictionPane(instrumentSS.getPowerRestriction());
      AnchorPaneHelper.insertNodeToContent(prPane.getPane(), powRestrictionPane);
   }

   private class PowerRestrictionPane {

      private BorderPane                      borderPane;
      private ListView<PowerRestrictionEntry> lwEntries;

      public PowerRestrictionPane(PowerRestriction restriction) {
         borderPane = new BorderPane();

         lwEntries = new ListView<>();
         lwEntries.getItems()
                  .addAll(restriction.getRestrictions());
         ListViewUtils.setCellFactory(lwEntries, this::entryToString);
         ListViewUtils.addItemSelectedListener(lwEntries,
                                               this::onEntrySelected);
         borderPane.setLeft(lwEntries);

         HBox hBox = new HBox(10);
         Button btnAdd = new Button("Add");
         btnAdd.setOnAction(this::btnAddPressed);
         Button btnRemove = new Button("Remove");
         btnRemove.setOnAction(this::btnRemovePressed);
         btnRemove.setTextFill(Color.RED);
         hBox.getChildren()
             .addAll(btnAdd, btnRemove);
         borderPane.setTop(hBox);
         BorderPane.setMargin(hBox, new Insets(5));
      }

      public BorderPane getPane() {
         return borderPane;
      }

      public PowerRestriction getRestriction() {
         return new PowerRestriction(lwEntries.getItems());
      }

      private String entryToString(PowerRestrictionEntry entry) {
         return String.format("T:[%d;%d) P:[%d;%d]", entry.getTempFromIncl(),
                              entry.getTempToExcl(), entry.getLowLimit(),
                              entry.getHighLimit());
      }

      private void onEntrySelected(PowerRestrictionEntry entry) {
         PowerRestrictionEntryForm form =
               new PowerRestrictionEntryForm(entry, lwEntries);
         borderPane.setCenter(form.getGraphics());
         BorderPane.setMargin(form.getGraphics(), new Insets(5));
      }

      private void btnAddPressed(ActionEvent event) {
         PowerRestrictionEntry entry =
               new PowerRestrictionEntry(400, 450, 10, 50);
         lwEntries.getItems()
                  .add(entry);
         onEntrySelected(entry);
      }

      private void btnRemovePressed(ActionEvent even) {
         PowerRestrictionEntry selectedEntry = lwEntries.getSelectionModel()
                                                        .getSelectedItem();
         lwEntries.getItems()
                  .remove(selectedEntry);
         borderPane.setCenter(new AnchorPane());
      }

   }

   private class PowerRestrictionEntryForm {

      private VBox vBox;

      public PowerRestrictionEntryForm(PowerRestrictionEntry entry,
                                       ListView<PowerRestrictionEntry> lwEntries) {
         vBox = new VBox(5);

         TextField edtTempFrom =
               new TextField(Integer.toString(entry.getTempFromIncl()));
         edtTempFrom.textProperty()
                    .addListener(new ChangeListener<String>() {

                       @Override
                       public void
                             changed(ObservableValue<? extends String> observable,
                                     String oldValue, String newValue) {
                          try {
                             entry.setTempFromIncl(Integer.parseInt(newValue));
                             refreshLw(lwEntries);
                          } catch (Exception e) {}
                       }
                    });
         vBox.getChildren()
             .addAll(new Label("Teplota od:"), edtTempFrom);

         TextField edtTempTo =
               new TextField(Integer.toString(entry.getTempToExcl()));
         edtTempTo.textProperty()
                  .addListener(new ChangeListener<String>() {

                     @Override
                     public void
                           changed(ObservableValue<? extends String> observable,
                                   String oldValue, String newValue) {
                        try {
                           entry.setTempToExcl(Integer.parseInt(newValue));
                           refreshLw(lwEntries);
                        } catch (Exception e) {}
                     }
                  });
         vBox.getChildren()
             .addAll(new Label("Teplota do:"), edtTempTo);

         TextField edtLowLimit =
               new TextField(Integer.toString(entry.getLowLimit()));
         edtLowLimit.textProperty()
                    .addListener(new ChangeListener<String>() {

                       @Override
                       public void
                             changed(ObservableValue<? extends String> observable,
                                     String oldValue, String newValue) {
                          try {
                             entry.setLowLimit(Integer.parseInt(newValue));
                             refreshLw(lwEntries);
                          } catch (Exception e) {}
                       }
                    });
         vBox.getChildren()
             .addAll(new Label("LowLimit:"), edtLowLimit);

         TextField edtHighLimit =
               new TextField(Integer.toString(entry.getHighLimit()));
         edtHighLimit.textProperty()
                     .addListener(new ChangeListener<String>() {

                        @Override
                        public void
                              changed(ObservableValue<? extends String> observable,
                                      String oldValue, String newValue) {
                           try {
                              entry.setHighLimit(Integer.parseInt(newValue));
                              refreshLw(lwEntries);
                           } catch (Exception e) {}
                        }
                     });
         vBox.getChildren()
             .addAll(new Label("HighLimit:"), edtHighLimit);
      }

      private void refreshLw(ListView<PowerRestrictionEntry> lwEntries) {
         List<PowerRestrictionEntry> items = lwEntries.getItems()
                                                      .stream()
                                                      .sorted(Comparator.comparing(PowerRestrictionEntry::getTempFromIncl))
                                                      .collect(Collectors.toList());
         lwEntries.getItems()
                  .clear();
         lwEntries.getItems()
                  .addAll(items);
      }

      private VBox getGraphics() {
         return vBox;
      }

   }

}
