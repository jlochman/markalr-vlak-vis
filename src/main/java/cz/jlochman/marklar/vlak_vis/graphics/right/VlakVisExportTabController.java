package cz.jlochman.marklar.vlak_vis.graphics.right;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import cz.jlochman.marklar.core.ServiceLocator;
import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.entityDomain.Sample;
import cz.jlochman.marklar.core.logicModules.definitions.ExperimentType;
import cz.jlochman.marklar.core.logicModules.settingsServices.ExpTypeSettingsService;
import cz.jlochman.marklar.fxcommon.ComplexTask;
import cz.jlochman.marklar.fxcommon.experiment.ExperimentInstanceController;
import cz.jlochman.marklar.fxcommon.layouts.ControllersLocator;
import cz.jlochman.marklar.fxcommon.logger.service.LogService;
import cz.jlochman.marklar.vlak_vis.export.VlakVisExportService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import lombok.extern.log4j.Log4j;

@Log4j
public class VlakVisExportTabController extends ExperimentInstanceController {

   @FXML
   private Button    btnExport, btnSelect;
   @FXML
   private TextField edtDirectory, edtFileName;
   @FXML
   private CheckBox  cbCSV, cbPDF;

   private ExpTypeSettingsService expTypeSS;
   private Sample                 sample;

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      btnSelect.setOnAction(this::chooseDirectoryPressed);
      btnExport.setOnAction(this::exportPressed);
   }

   @Override
   public void setExpInstance(ExperimentInstance expInstance) {
      super.setExpInstance(expInstance);
      sample = expInstance.getSample();

      expTypeSS = expInstance.getExperimentType()
                             .getExpDefaultSettingsService();

      edtDirectory.setText(expTypeSS.getDataExportPath());
      edtFileName.setText(String.format("%05d", expInstance.getId()));

   }

   @Override
   public void repaint() {
      // TODO Auto-generated method stub

   }

   @Override
   public void newData(Object dataRow) {
      // TODO Auto-generated method stub

   }

   private void chooseDirectoryPressed(ActionEvent event) {
      final DirectoryChooser directoryChooser = new DirectoryChooser();
      final File selectedDirectory = directoryChooser.showDialog(new Stage());
      if (selectedDirectory != null) {
         String path = selectedDirectory.getAbsolutePath();
         edtDirectory.setText(path);
         expTypeSS.setDataExportPath(path);
      }
   }

   private void exportPressed(ActionEvent event) {
      String directoryName = edtDirectory.getText() + File.separator
                             + edtFileName.getText() + File.separator;
      File directory = new File(directoryName);
      if (!directory.exists()) {
         try {
            directory.mkdirs();
         } catch (Exception e) {
            LogService.getInstance()
                      .info("Error during dir creation " + directoryName);
            e.printStackTrace();
         }
      }
      String fileName =
            directoryName + String.format("%05d", expInstance.getId());

      Map<ExperimentType, List<ExperimentInstance>> map =
            ServiceLocator.getInstance()
                          .getExperimentService()
                          .getGroupedExperimentForSample(sample);
      if (map.size() != 1) {
         log.error("grouped experiment map size greater than 1");
         return;
      }
      List<ExperimentInstance> expInstances = map.values()
                                                 .stream()
                                                 .findFirst()
                                                 .get();
      VlakVisExportService exportService =
            (VlakVisExportService) expInstance.getExperimentType()
                                              .getExperimentExportService(expInstances);

      if (cbCSV.isSelected()) {
         try {
            exportService.exportAsCSV(fileName);
            LogService.getInstance()
                      .info("CSV created successfully");
         } catch (IOException e) {
            LogService.getInstance()
                      .info("CSV wasn't created. Does target dir exist? Does it correct rights?");
            e.printStackTrace();
         }
      }

      if (cbPDF.isSelected()) {
         ComplexTask task = new ComplexTask() {

            @Override
            protected void runThis() {
               try {
                  exportService.exportAsPDF(fileName);
                  LogService.getInstance()
                            .info("PDF created successfully");
               } catch (IOException e) {
                  LogService.getInstance()
                            .info("PDF wasn't created. Does target dir exist? Does it correct rights?");
                  e.printStackTrace();
               }
            }
         };
         ControllersLocator.getInstance()
                           .getStandardLayoutController()
                           .getMainPane()
                           .disableProperty()
                           .bind(task.runningProperty());
         task.start();
      }

   }

}
