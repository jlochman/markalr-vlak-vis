package cz.jlochman.marklar.vlak_vis.graphics;

import java.util.List;

import cz.jlochman.marklar.core.graphicsDefinitions.AnchorPaneHelper;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataRow;
import cz.jlochman.marklar.vlak_vis.data.VlakVisDataService;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.layout.AnchorPane;

/**
 * trida ridici vykreslovani celeho grafu
 * 
 * @author jlochman
 */
public class VlakVisChart {

   private AnchorPane pane;

   private LineChart<Number, Number> chart;
   private Series<Number, Number>    speedSerie;
   private boolean                   isTiny;

   public VlakVisChart(AnchorPane pane, boolean isTiny) {
      this.pane = pane;
      this.isTiny = isTiny;

      createGraph();
      AnchorPaneHelper.insertNodeToContent(chart, pane);
   }

   public VlakVisChart(boolean isTiny) {
      this.pane = null;
      this.isTiny = isTiny;

      createGraph();
   }

   public void repaint(VlakVisDataService dataService) {
      List<VlakVisDataRow> dataRows = dataService.getDataRows();
      if (dataRows == null || dataRows.isEmpty()) {
         return;
      }

      for (VlakVisDataRow dr : dataRows) {
         addData(dr);
      }

   }

   private double xMin = 1000, xMax = -1000;
   private double yMin = 1000, yMax = -1000;

   private static double roundToTens(double x) {
      return Math.round(x / 10.0) * 10.0;
   }

   public void addData(VlakVisDataRow dataRow) {
      double x = dataRow.getTemp();
      if (x < xMin) {
         xMin = roundToTens(x) - 10;
      }
      if (x > xMax) {
         xMax = roundToTens(x) + 10;
      }
      ((NumberAxis) chart.getXAxis()).setLowerBound(xMin);
      ((NumberAxis) chart.getXAxis()).setUpperBound(xMax);

      double y = dataRow.getSpeedLogMmMin();
      if (y < yMin) {
         yMin = Math.round(y) - 1;
      }
      if (y > yMax) {
         yMax = Math.round(y) + 1;
      }
      ((NumberAxis) chart.getYAxis()).setLowerBound(yMin);
      ((NumberAxis) chart.getYAxis()).setUpperBound(yMax);

      speedSerie.getData()
                .add(new Data<>(x, y));
      chart.getData()
           .clear();
      chart.getData()
           .add(speedSerie);
   }

   private void createGraph() {
      NumberAxis xAxis = new NumberAxis();
      xAxis.setAnimated(false);
      xAxis.setAutoRanging(false);
      xAxis.setLowerBound(0);
      xAxis.setUpperBound(600);
      xAxis.setTickUnit(50);
      if (!isTiny) {
         xAxis.setLabel("Teplota [˚C]");
      }

      NumberAxis yAxis = new NumberAxis();
      yAxis.setAnimated(false);
      yAxis.setAutoRanging(false);
      yAxis.setLowerBound(-50.);
      yAxis.setUpperBound(100.);
      if (isTiny) {
         yAxis.setTickUnit(50.);
      } else {
         yAxis.setTickUnit(10.);
      }
      if (!isTiny) {
         yAxis.setLabel("Log rychlosti [log(mm/min)]");
      }

      chart = new LineChart<Number, Number>(xAxis, yAxis);
      chart.getData()
           .clear();

      speedSerie = new Series<>();
      chart.getData()
           .add(speedSerie);

      chart.setAnimated(false);
      chart.setLegendVisible(false);
      chart.setCreateSymbols(true);
      chart.getStylesheets()
           .add(getClass().getResource("chart-style.css")
                          .toExternalForm());

      if (this.pane != null) {
         AnchorPaneHelper.insertNodeToContent(chart, pane);
      }
   }

   public LineChart<Number, Number> getChart() {
      return chart;
   }

}
