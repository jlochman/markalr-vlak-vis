package cz.jlochman.marklar.vlak_vis.graphics.cells;

import java.net.URL;
import java.util.ResourceBundle;

import cz.jlochman.marklar.core.entityDomain.ExperimentInstance;
import cz.jlochman.marklar.core.experiments.common.ExperimentStatus;
import cz.jlochman.marklar.core.graphicsDefinitions.GenericCell;
import cz.jlochman.marklar.core.helpers.DecimalHelper;
import cz.jlochman.marklar.vlak_vis.experiment.settings.VlakVisInstanceSettingsService;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class VlakVisPreparedCellController extends GenericCell {

   @FXML
   private ImageView imgIcon;
   @FXML
   private Label     lblMessId, lblStartT, lblLength, lblDiameter;

   private VlakVisInstanceSettingsService instanceSS;

   public VlakVisPreparedCellController(ExperimentInstance expInst) {
      super(expInst);
      setFXMLFile("VlakVisPreparedCell.fxml");

      instanceSS =
            (VlakVisInstanceSettingsService) expInst.getSettignsService();
   }

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      lblMessId.setText("");
      lblStartT.setText("");
      lblLength.setText("");
      lblDiameter.setText("");
   }

   @Override
   public void fillGraphics() {
      setBackgroundColor();
      setImage();

      lblMessId.setText(Long.toString(expInst.getId()));
      lblStartT.setText(DecimalHelper.toString(instanceSS.getStartT(), 1)
                        + " ˚C");
      lblLength.setText(DecimalHelper.toString(instanceSS.getLength(), 3)
                        + " mm");
      lblDiameter.setText(DecimalHelper.toString(instanceSS.getDiameter(), 3)
                          + " mm");
   }

   private void setImage() {
      if (expInst.getStatus() == ExperimentStatus.NEW) {
         imgIcon.setImage(new Image(this.getClass()
                                        .getResourceAsStream("unlock.png")));
      } else if (expInst.getStatus() == ExperimentStatus.NEW_LOCKED) {
         imgIcon.setImage(new Image(this.getClass()
                                        .getResourceAsStream("lock.png")));
      }
   }

}
