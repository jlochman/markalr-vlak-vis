package cz.jlochman.marklar.vlak_vis;

import java.util.List;

import cz.jlochman.marklar.core.ServiceLocator;
import cz.jlochman.marklar.core.configuration.initializer.ConfigurationInitializer;
import cz.jlochman.marklar.core.instruments.Instrument;
import cz.jlochman.marklar.fxcommon.layouts.StandardFadeApp;

public class VlakVisApp extends StandardFadeApp {

   public static void main(String[] args) {
      savedArgs = args;
      launch(args);
   }

   private static VlakVisInitializer initializer = null;

   @Override
   protected ConfigurationInitializer getConfigurationInitializer() {
      if (initializer == null) {
         initializer = new VlakVisInitializer();
      }
      return initializer;
   }

   @Override
   protected void terminateProcedure() {
      List<Instrument> instruments = ServiceLocator.getInstance()
                                                   .getInstrumentService()
                                                   .getInstrumentList();

      if (instruments == null || instruments.isEmpty()) {
         return;
      }

      instruments.stream()
                 .forEach(Instrument::terminateProcedure);
   }

}
