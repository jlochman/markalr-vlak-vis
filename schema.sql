# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.17)
# Database: marklar_plastometer
# Generation Time: 2018-06-11 10:17:11 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table experiment_instance
# ------------------------------------------------------------

DROP TABLE IF EXISTS `experiment_instance`;

CREATE TABLE `experiment_instance` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  `DESCRIPTION` longtext COLLATE utf8_czech_ci,
  `experimentInstrumentId` bigint(20) NOT NULL,
  `experimentTypeId` bigint(20) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `OPERATOR` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `STATUS` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `SAMPLE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKpbjv40tku9bsidk2yuo3qwqpv` (`SAMPLE_ID`),
  CONSTRAINT `FKpbjv40tku9bsidk2yuo3qwqpv` FOREIGN KEY (`SAMPLE_ID`) REFERENCES `sample` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



# Dump of table experiment_instance_final_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `experiment_instance_final_data`;

CREATE TABLE `experiment_instance_final_data` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `VALUE` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `INSTANCE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKm6it8f5b5pkdiujhruexytg45` (`INSTANCE_ID`),
  CONSTRAINT `FKm6it8f5b5pkdiujhruexytg45` FOREIGN KEY (`INSTANCE_ID`) REFERENCES `experiment_instance` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



# Dump of table experiment_instance_timed_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `experiment_instance_timed_data`;

CREATE TABLE `experiment_instance_timed_data` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  `EXPERIMENT_TIME` bigint(20) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `VALUE` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `INSTANCE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK4hfc6g87t38gg3ob93fgfdjyo` (`INSTANCE_ID`),
  CONSTRAINT `FK4hfc6g87t38gg3ob93fgfdjyo` FOREIGN KEY (`INSTANCE_ID`) REFERENCES `experiment_instance` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



# Dump of table sample
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sample`;

CREATE TABLE `sample` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  `DESCRIPTION` longtext COLLATE utf8_czech_ci,
  `NAME` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `OBJECT_ID` bigint(20) DEFAULT NULL,
  `SETTING_TYPE` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `VALUE` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
